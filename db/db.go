package db

import (
	// "ApiDashboard/config"
	"fmt"

	"github.com/jinzhu/gorm"
	// For postgres connection
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"os"
)
var db *gorm.DB
var err error

// Init DB Connection
func Init() {
	// configuration := config.GetConfig()
	// fmt.Printf(configuration.DB_HOST)
	// fmt.Printf(os.Getenv("DB_HOST"))
	connectString := fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=disable", 
	os.Getenv("DB_HOST"), 
	os.Getenv("DB_PORT"), 
	os.Getenv("DB_USERNAME"), 
	os.Getenv("DB_NAME"), 
	os.Getenv("DB_PASSWORD"))

	db, err = gorm.Open("postgres", connectString)
	
	// dsn := "host=localhost user=vianto password=Vianto1125 dbname=db_latihan1 port=5432 sslmode=disable TimeZone=Asia/Shanghai"
  // db, err := gorm.Open(postgres.Open(dsn), &gorm.DB{})

	// defer db.Close()
	if err != nil {
		panic(err.Error())
		// panic("DB Connection Error cuy")
	}
	// db.AutoMigrate(&model.Notifikasi{})
}
// Manager for conn
func Manager() *gorm.DB {
	return db
}
// ManagerRetrieve return db connection
func ManagerRetrieve() *gorm.DB {
	return db
}
FROM golang:1.17

# RUN apk update && apk add --no-cache git

WORKDIR /bin

COPY . .

RUN go mod tidy

RUN go build -o binary

ENTRYPOINT ["/bin/binary"]
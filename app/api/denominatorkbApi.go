package api

import (
	"ApiDashboard/db"
	"ApiDashboard/model"
	"net/http"

	"github.com/labstack/echo/v4"
)

func GetWilayahDenoKb(c echo.Context) error {
	prov := c.QueryParam("prov")
	kab := c.QueryParam("kota")
	kec := c.QueryParam("kec")
	kel := c.QueryParam("kel")
	limit := c.QueryParam("limit")
	offset := c.QueryParam("offset")
	order := c.QueryParam("order")
	DbCon := db.Manager()
	denoKB := []model.DenominatorKb{}
	if limit == "" {
		limit = "20"
	}
	if offset == "" {
		offset = "0"
	}
	if order == "" {
		order = "nama desc"
	}

	if kel != "" {
		dbc := DbCon.Debug().Where("kelurahan_id = ?", kel).Limit(limit).Offset(offset).Order(order).Find(&denoKB)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kec != "" {
		dbc := DbCon.Debug().Where("kecamatan_id = ?", kec).Limit(limit).Offset(offset).Order(order).Find(&denoKB)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kab != "" {
		dbc := DbCon.Debug().Where("kota_kabupaten_id = ?", kab).Limit(limit).Offset(offset).Order(order).Find(&denoKB)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if prov != "" {
		dbc := DbCon.Debug().Where("provinsi_id = ?", prov).Limit(limit).Offset(offset).Order(order).Find(&denoKB)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else {
		dbc := DbCon.Debug().Limit(limit).Offset(offset).Order(order).Find(&denoKB)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	}
	if denoKB == nil {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":   "false",
			"message": "Data Kosong",
		})
	} else {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":   false,
			"message": "Data Berhasil di ambil",
			"data":    denoKB,
		})
	}
}

func CountGetWilayahDenoKb(c echo.Context) error {
	// var WANITA_KAWIN_TIDAK_HAMIL_UMUR_10_sd_14 int
	// var WANITA_KAWIN_TIDAK_HAMIL_UMUR_15_sd_49 int
	// var WANITA_KAWIN_TIDAK_HAMIL_UMUR_50_sd_54 int
	// var countSurveyIndividu int
	prov := c.QueryParam("prov")
	kab := c.QueryParam("kota")
	kec := c.QueryParam("kec")
	kel := c.QueryParam("kel")
	DbCon := db.Manager()
	denoKB := []model.DenominatorKb{}

	if kel != "" {
		dbc := DbCon.Debug().Raw("SELECT count(survei_individu_detail_id) as survei_individu_detail_id, sum(wanita_kawin_tidak_hamil_umur_10_sd_14) as wanita_kawin_tidak_hamil_umur_10_sd_14, sum(wanita_kawin_tidak_hamil_umur_15_sd_49) as wanita_kawin_tidak_hamil_umur_15_sd_49,sum(wanita_kawin_tidak_hamil_umur_50_sd_54) as wanita_kawin_tidak_hamil_umur_50_sd_54 from denominator_kb where kota_kelurahan_id =?",kel).Scan(&denoKB)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kec != "" {
		dbc := DbCon.Debug().Raw("SELECT count(survei_individu_detail_id) as survei_individu_detail_id, sum(wanita_kawin_tidak_hamil_umur_10_sd_14) as wanita_kawin_tidak_hamil_umur_10_sd_14, sum(wanita_kawin_tidak_hamil_umur_15_sd_49) as wanita_kawin_tidak_hamil_umur_15_sd_49,sum(wanita_kawin_tidak_hamil_umur_50_sd_54) as wanita_kawin_tidak_hamil_umur_50_sd_54 from denominator_kb where kecamatan_id =?",kec).Scan(&denoKB)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kab != "" {
		dbc := DbCon.Debug().Raw("SELECT count(survei_individu_detail_id) as survei_individu_detail_id, sum(wanita_kawin_tidak_hamil_umur_10_sd_14) as wanita_kawin_tidak_hamil_umur_10_sd_14, sum(wanita_kawin_tidak_hamil_umur_15_sd_49) as wanita_kawin_tidak_hamil_umur_15_sd_49,sum(wanita_kawin_tidak_hamil_umur_50_sd_54) as wanita_kawin_tidak_hamil_umur_50_sd_54 from denominator_kb where kota_kabupaten_id =?",kab).Scan(&denoKB)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if prov != "" {
		dbc := DbCon.Debug().Raw("SELECT count(survei_individu_detail_id) as survei_individu_detail_id, sum(wanita_kawin_tidak_hamil_umur_10_sd_14) as wanita_kawin_tidak_hamil_umur_10_sd_14, sum(wanita_kawin_tidak_hamil_umur_15_sd_49) as wanita_kawin_tidak_hamil_umur_15_sd_49,sum(wanita_kawin_tidak_hamil_umur_50_sd_54) as wanita_kawin_tidak_hamil_umur_50_sd_54 from denominator_kb where provinsi_id =?",prov).Scan(&denoKB)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else {
		dbc := DbCon.Debug().Raw("SELECT count(survei_individu_detail_id) as survei_individu_detail_id, sum(wanita_kawin_tidak_hamil_umur_10_sd_14) as wanita_kawin_tidak_hamil_umur_10_sd_14, sum(wanita_kawin_tidak_hamil_umur_15_sd_49) as wanita_kawin_tidak_hamil_umur_15_sd_49, sum(wanita_kawin_tidak_hamil_umur_50_sd_54) as wanita_kawin_tidak_hamil_umur_50_sd_54 FROM denominator_kb").Find(&denoKB)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	}
	if denoKB == nil {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":   "false",
			"message": "Data Kosong",
		})
	} else {
		// for i := range denoKB {
		// 	if denoKB[i].WANITA_KAWIN_TIDAK_HAMIL_UMUR_10_sd_14 == 1 {
		// 		WANITA_KAWIN_TIDAK_HAMIL_UMUR_10_sd_14 += 1
		// 	}
		// 	if denoKB[i].WANITA_KAWIN_TIDAK_HAMIL_UMUR_15_sd_49 == 1 {
		// 		WANITA_KAWIN_TIDAK_HAMIL_UMUR_15_sd_49 += 1
		// 	}

		// 	if denoKB[i].WANITA_KAWIN_TIDAK_HAMIL_UMUR_50_sd_54 == 1 {
		// 		WANITA_KAWIN_TIDAK_HAMIL_UMUR_50_sd_54 += 1
		// 	}
		// 	if denoKB[i].Survei_individu_detail_id != "" {
		// 		countSurveyIndividu += 1
		// 	}
		// }
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":                     false,
			"message":                   "Data Berhasil di ambil",
			"survei_individu_detail_id": denoKB[0].Survei_individu_detail_id,
			"jumlah_wanita_kawin_tidak_hamil_umur_10_sd_14": denoKB[0].Wanita_kawin_tidak_hamil_umur_10_sd_14,
			"jumlah_wanita_kawin_tidak_hamil_umur_15_sd_49":  denoKB[0].Wanita_kawin_tidak_hamil_umur_15_sd_49,
			"jumlah_wanita_kawin_tidak_hamil_umur_50_sd_54":  denoKB[0].Wanita_kawin_tidak_hamil_umur_50_sd_54,
		})
	}
}

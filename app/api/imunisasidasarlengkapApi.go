package api

import (
	"ApiDashboard/db"
	"ApiDashboard/model"
	"net/http"

	"github.com/labstack/echo/v4"
)

func GetWilayahImunisasiDasar(c echo.Context) error {
	prov := c.QueryParam("prov")
	kab := c.QueryParam("kota")
	kec := c.QueryParam("kec")
	kel := c.QueryParam("kel")
	limit := c.QueryParam("limit")
	offset := c.QueryParam("offset")
	order := c.QueryParam("order")
	DbCon := db.Manager()
	imunisasi := []model.ImunisasiDasarLengkap{}
	if limit == "" {
		limit = "20"
	}
	if offset == "" {
		offset = "0"
	}
	if order == "" {
		order = "nama desc"
	}

	if kel != "" {
		dbc := DbCon.Debug().Where("kelurahan_id = ?", kel).Limit(limit).Offset(offset).Order(order).Find(&imunisasi)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kec != "" {
		dbc := DbCon.Debug().Where("kecamatan_id = ?", kec).Limit(limit).Offset(offset).Order(order).Find(&imunisasi)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kab != "" {
		dbc := DbCon.Debug().Where("kota_kabupaten_id = ?", kab).Limit(limit).Offset(offset).Order(order).Find(&imunisasi)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if prov != "" {
		dbc := DbCon.Debug().Where("provinsi_id = ?", prov).Limit(limit).Offset(offset).Order(order).Find(&imunisasi)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else {
		dbc := DbCon.Debug().Limit(limit).Offset(offset).Order(order).Find(&imunisasi)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	}
	if imunisasi == nil {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":   "false",
			"message": "Data Kosong",
		})
	} else {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":   false,
			"message": "Data Berhasil di ambil",
			"data":    imunisasi,
		})
	}
}

func CountGetWilayahImunisasiDasar(c echo.Context) error {

	prov := c.QueryParam("prov")
	kab := c.QueryParam("kota")
	kec := c.QueryParam("kec")
	kel := c.QueryParam("kel")
	DbCon := db.Manager()
	imunisasi := []model.ImunisasiDasarLengkap{}
	if kel != "" {
		dbc := DbCon.Debug().Raw("select sum(memiliki_balita_12_sd_23_bulan) as memiliki_balita_12_sd_23_bulan,sum(sasaran_tidak_idl) as sasaran_tidak_idl,count(survei_individu_detail_id) as survei_individu_detail_id from imunisasi_dasar_lengkap where kelurahan_id = ?",kel).Scan(&imunisasi)

		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}

	} else if kec != "" {
		dbc := DbCon.Debug().Raw("select sum(memiliki_balita_12_sd_23_bulan) as memiliki_balita_12_sd_23_bulan,sum(sasaran_tidak_idl) as sasaran_tidak_idl,count(survei_individu_detail_id) as survei_individu_detail_id from imunisasi_dasar_lengkap where kecamatan_id = ?",kec).Scan(&imunisasi)

		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}

	} else if kab != "" {
		dbc := DbCon.Debug().Raw("select sum(memiliki_balita_12_sd_23_bulan) as memiliki_balita_12_sd_23_bulan,sum(sasaran_tidak_idl) as sasaran_tidak_idl,count(survei_individu_detail_id) as survei_individu_detail_id from imunisasi_dasar_lengkap where kota_kabupaten_id= ?",kab).Scan(&imunisasi)

		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}

	} else if prov != "" {
		dbc := DbCon.Debug().Raw("select sum(memiliki_balita_12_sd_23_bulan) as memiliki_balita_12_sd_23_bulan,sum(sasaran_tidak_idl) as sasaran_tidak_idl,count(survei_individu_detail_id) as survei_individu_detail_id from imunisasi_dasar_lengkap where provinsi_id = ?",prov).Scan(&imunisasi)

		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}

	} else {
		dbc := DbCon.Debug().Raw("select sum(memiliki_balita_12_sd_23_bulan) as memiliki_balita_12_sd_23_bulan,sum(sasaran_tidak_idl) as sasaran_tidak_idl,count(survei_individu_detail_id) as survei_individu_detail_id from imunisasi_dasar_lengkap").Scan(&imunisasi)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	}
	if imunisasi == nil {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":   "false",
			"message": "Data Kosong",
		})
	} else {

		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":                                 false,
			"message":                               "Data Berhasil di ambil",
			"jumlah_survei_individu_detail_id":      imunisasi[0].Survei_individu_detail_id,
			"jumlah_sasaran_tidak_idl":              imunisasi[0].Sasaran_tidak_idl,
			"jumlah_memiliki_balita_12_sd_23_bulan": imunisasi[0].Memiliki_balita_12_sd_23_bulan,
		})
	}
}

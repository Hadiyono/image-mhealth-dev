package api

import (
	"ApiDashboard/db"
	"ApiDashboard/model"
	"net/http"

	"github.com/labstack/echo/v4"
)

func GetWilayahPersalinanFKS(c echo.Context) error {
	prov := c.QueryParam("prov")
	kab := c.QueryParam("kota")
	kec := c.QueryParam("kec")
	kel := c.QueryParam("kel")
	limit := c.QueryParam("limit")
	offset := c.QueryParam("offset")
	order := c.QueryParam("order")
	DbCon := db.Manager()
	pdfaskes := []model.PersalinanDifaskes{}
	if limit == "" {
		limit = "20"
	}
	if offset == "" {
		offset = "0"
	}
	if order == "" {
		order = "nama desc"
	}

	if kel != "" {
		dbc := DbCon.Debug().Where("kelurahan_id = ?", kel).Limit(limit).Offset(offset).Order(order).Find(&pdfaskes)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kec != "" {
		dbc := DbCon.Debug().Where("kecamatan_id = ?", kec).Limit(limit).Offset(offset).Order(order).Find(&pdfaskes)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kab != "" {
		dbc := DbCon.Debug().Where("kota_kabupaten_id = ?", kab).Limit(limit).Offset(offset).Order(order).Find(&pdfaskes)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if prov != "" {
		dbc := DbCon.Debug().Where("provinsi_id = ?", prov).Limit(limit).Offset(offset).Order(order).Find(&pdfaskes)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else {
		dbc := DbCon.Debug().Limit(limit).Offset(offset).Order(order).Find(&pdfaskes)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	}
	if pdfaskes == nil {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":   "false",
			"message": "Data Kosong",
		})
	} else {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":   false,
			"message": "Data Berhasil di ambil",
			"data":    pdfaskes,
		})
	}
}

func CountGetWilayahPersalinanFKS(c echo.Context) error {
	prov := c.QueryParam("prov")
	kab := c.QueryParam("kota")
	kec := c.QueryParam("kec")
	kel := c.QueryParam("kel")
	DbCon := db.Manager()
	pdfaskes := []model.PersalinanDifaskes{}
	// var IBU_DENGAN_ANAK_USIA_0_sd_11_BULAN int
	// var PERSALINAN_TIDAK_DI_FASKES int
	// var Survei_individu_detail_id int

	if kel != "" {
		dbc := DbCon.Debug().Raw("select count(survei_individu_detail_id) as survei_individu_detail_id,sum(ibu_dengan_anak_usia_0_sd_11_bulan) as ibu_dengan_anak_usia_0_sd_11_bulan,sum(persalinan_tidak_di_faskes) as persalinan_tidak_di_faskes from persalinan_di_faskes where kelurahan_id=?",kel).Scan(&pdfaskes)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kec != "" {
		dbc := DbCon.Debug().Raw("select count(survei_individu_detail_id) as survei_individu_detail_id,sum(ibu_dengan_anak_usia_0_sd_11_bulan) as ibu_dengan_anak_usia_0_sd_11_bulan,sum(persalinan_tidak_di_faskes) as persalinan_tidak_di_faskes from persalinan_di_faskes where kecamatan_id=?",kec).Scan(&pdfaskes)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kab != "" {
		dbc := DbCon.Debug().Raw("select count(survei_individu_detail_id) as survei_individu_detail_id,sum(ibu_dengan_anak_usia_0_sd_11_bulan) as ibu_dengan_anak_usia_0_sd_11_bulan,sum(persalinan_tidak_di_faskes) as persalinan_tidak_di_faskes from persalinan_di_faskes where kota_kabupaten_id=?",kab).Scan(&pdfaskes)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if prov != "" {
		dbc := DbCon.Debug().Raw("select count(survei_individu_detail_id) as survei_individu_detail_id,sum(ibu_dengan_anak_usia_0_sd_11_bulan) as ibu_dengan_anak_usia_0_sd_11_bulan,sum(persalinan_tidak_di_faskes) as persalinan_tidak_di_faskes from persalinan_di_faskes where provinsi_id=?",prov).Scan(&pdfaskes)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else {
		dbc := DbCon.Debug().Raw("select count(survei_individu_detail_id) as survei_individu_detail_id,sum(ibu_dengan_anak_usia_0_sd_11_bulan) as ibu_dengan_anak_usia_0_sd_11_bulan,sum(persalinan_tidak_di_faskes) as persalinan_tidak_di_faskes from persalinan_di_faskes").Scan(&pdfaskes)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	}
	if pdfaskes == nil {
		return c.JSON(http.StatusNoContent, map[string]interface{}{
			"error":   "false",
			"message": "Data Kosong",
		})
	} else {
		
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":                            false,
			"message":                          "Data Berhasil di ambil",
			"jumlah_survei_individu_detail_id": pdfaskes[0].Survei_individu_detail_id,
			"jumlah_ibu_dengan_anak_usia_0_sd_11_bulan": pdfaskes[0].Ibu_dengan_anak_usia_0_sd_11_bulan,
			"jumlah_persalinan_tidak_di_faskes":         pdfaskes[0].Persalinan_tidak_di_faskes,
		})
	}
}

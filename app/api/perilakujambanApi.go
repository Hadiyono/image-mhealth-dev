package api

import (
	"ApiDashboard/db"
	"ApiDashboard/model"
	"net/http"

	"github.com/labstack/echo/v4"
)

func GetWilayahPJamban(c echo.Context) error {
	prov := c.QueryParam("prov")
	kab := c.QueryParam("kota")
	kec := c.QueryParam("kec")
	kel := c.QueryParam("kel")
	limit := c.QueryParam("limit")
	offset := c.QueryParam("offset")
	order := c.QueryParam("order")
	DbCon := db.Manager()
	PJamban := []model.PerilakuJamban{}
	if limit == "" {
		limit = "20"
	}
	if offset == "" {
		offset = "0"
	}
	if order == "" {
		order = "nama desc"
	}

	if kel != "" {
		dbc := DbCon.Debug().Where("kelurahan_id = ?", kel).Limit(limit).Offset(offset).Order(order).Find(&PJamban)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kec != "" {
		dbc := DbCon.Debug().Where("kecamatan_id = ?", kec).Limit(limit).Offset(offset).Order(order).Find(&PJamban)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kab != "" {
		dbc := DbCon.Debug().Where("kota_kabupaten_id = ?", kab).Limit(limit).Offset(offset).Order(order).Find(&PJamban)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if prov != "" {
		dbc := DbCon.Debug().Where("provinsi_id = ?", prov).Limit(limit).Offset(offset).Order(order).Find(&PJamban)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else {
		dbc := DbCon.Debug().Limit(limit).Offset(offset).Order(order).Find(&PJamban)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	}
	if PJamban == nil {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":   "false",
			"message": "Data Kosong",
		})
	} else {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":   false,
			"message": "Data Berhasil di ambil",
			"data":    PJamban,
		})
	}
}

func CountGetWilayahPJamban(c echo.Context) error {
	// var IND_PUNYA_JAMBAN_SANITER_PERILAKU int
	// var countSurveyIndividu int
	prov := c.QueryParam("prov")
	kab := c.QueryParam("kota")
	kec := c.QueryParam("kec")
	kel := c.QueryParam("kel")
	DbCon := db.Manager()
	PJamban := []model.PerilakuJamban{}

	if kel != "" {
		dbc := DbCon.Debug().Raw("select sum(ind_punya_jamban_saniter_perilaku) as ind_punya_jamban_saniter_perilaku,count(survei_individu_detail_id) as survei_individu_detail_id from perilaku_jamban where kelurahan_id = ?", kel).Scan(&PJamban)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kec != "" {
		dbc := DbCon.Debug().Raw("select sum(ind_punya_jamban_saniter_perilaku) as ind_punya_jamban_saniter_perilaku,count(survei_individu_detail_id) as survei_individu_detail_id from perilaku_jamban where kecamatan_id = ?", kec).Scan(&PJamban)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kab != "" {
		// dbc := DbCon.Debug().Select("ind_punya_jamban_saniter_perilaku,survei_individu_detail_id").Where("kota_kabupaten_id = ?", kab).Find(&PJamban)
		
		// select count and sum
		dbc := DbCon.Debug().Raw("select sum(ind_punya_jamban_saniter_perilaku) as ind_punya_jamban_saniter_perilaku,count(survei_individu_detail_id) as survei_individu_detail_id from perilaku_jamban where kota_kabupaten_id = ?", kab).Scan(&PJamban)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if prov != "" {
		// dbc := DbCon.Debug().Select("ind_punya_jamban_saniter_perilaku,survei_individu_detail_id").Where("provinsi_id = ?", prov).Find(&PJamban)
		// select count and sum
		dbc := DbCon.Debug().Raw("select sum(ind_punya_jamban_saniter_perilaku) as ind_punya_jamban_saniter_perilaku, count(survei_individu_detail_id) as survei_individu_detail_id from perilaku_jamban where provinsi_id = ?", prov).Scan(&PJamban)
		
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else {
		//select count sum scan(&Pjamban)
		dbc := DbCon.Debug().Raw("select sum(ind_punya_jamban_saniter_perilaku) as ind_punya_jamban_saniter_perilaku,count(survei_individu_detail_id) as survei_individu_detail_id from perilaku_jamban").Scan(&PJamban)

		// dbc := DbCon.Debug().Select("ind_punya_jamban_saniter_perilaku,survei_individu_detail_id").Find(&PJamban)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	}
	if PJamban == nil {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":   "false",
			"message": "Data Kosong",
		})
	} else {
		
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":                            false,
			"message":                          "Data Berhasil di ambil",
			"jumlah_survei_individu_detail_id": PJamban[0].Survei_individu_detail_id,
			"jumlah_ind_punya_jambansaniter":   PJamban[0].Ind_punya_jamban_saniter_perilaku,
		})
	}
}

package api

import (
	"ApiDashboard/db"
	"ApiDashboard/model"
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"
)

func GetWilayahAsiEsk(c echo.Context) error {
	prov := c.QueryParam("prov")
	kab := c.QueryParam("kota")
	kec := c.QueryParam("kec")
	kel := c.QueryParam("kel")
	limit := c.QueryParam("limit")
	offset := c.QueryParam("offset")
	order := c.QueryParam("order")
	DbCon := db.Manager()
	AsiEsk := []model.Asieksklusif{}
	if limit == "" {
		limit = "20"
	}
	if offset == "" {
		offset = "0"
	}
	if order == "" {
		order = "nama desc"
	}

	if kel != "" {
		dbc := DbCon.Debug().Where("kelurahan_id = ?", kel).Limit(limit).Offset(offset).Order(order).Find(&AsiEsk)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kec != "" {
		dbc := DbCon.Debug().Where("kecamatan_id = ?", kec).Limit(limit).Offset(offset).Order(order).Find(&AsiEsk)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kab != "" {
		dbc := DbCon.Debug().Where("kota_kabupaten_id = ?", kab).Limit(limit).Offset(offset).Order(order).Find(&AsiEsk)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if prov != "" {
		dbc := DbCon.Debug().Where("provinsi_id = ?", prov).Limit(limit).Offset(offset).Order(order).Find(&AsiEsk)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else {
		dbc := DbCon.Debug().Limit(limit).Offset(offset).Order(order).Find(&AsiEsk)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	}
	if AsiEsk == nil {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":   "false",
			"message": "Data Kosong",
		})
	} else {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":   false,
			"message": "Data Berhasil di ambil",
			"data":    AsiEsk,
		})
	}
}

func CountGetWilayahAsiEsk(c echo.Context) error {
	prov := c.QueryParam("prov")
	kab := c.QueryParam("kota")
	kec := c.QueryParam("kec")
	kel := c.QueryParam("kel")
	DbCon := db.Manager()
	AsiEsk := []model.Asieksklusif{}
	// var countMemilikiBayi int
	// var countTidakAsiEkslisif int
	// var countSurveyIndividu int
	if kel != "" {
		dbc := DbCon.Debug().Raw("SELECT count(survei_individu_detail_id) as survei_individu_detail_id, sum(memiliki_bayi_7_sd_23_bulan) as memiliki_bayi_7_sd_23_bulan, sum(sasaran_tidak_asi_ekslusif) as sasaran_tidak_asi_ekslusif FROM asi_eksklusif where kelurahan_id= ?", kel).Scan(&AsiEsk)

		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kec != "" {
		dbc := DbCon.Debug().Raw("SELECT count(survei_individu_detail_id) as survei_individu_detail_id, sum(memiliki_bayi_7_sd_23_bulan) as memiliki_bayi_7_sd_23_bulan, sum(sasaran_tidak_asi_ekslusif) as sasaran_tidak_asi_ekslusif FROM asi_eksklusif where kecamatan_id= ?", kec).Scan(&AsiEsk)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kab != "" {
		dbc := DbCon.Debug().Raw("SELECT count(survei_individu_detail_id) as survei_individu_detail_id, sum(memiliki_bayi_7_sd_23_bulan) as memiliki_bayi_7_sd_23_bulan, sum(sasaran_tidak_asi_ekslusif) as sasaran_tidak_asi_ekslusif FROM asi_eksklusif where kota_kabupaten_id= ?",kab).Scan(&AsiEsk)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if prov != "" {
		dbc := DbCon.Debug().Raw("SELECT count(survei_individu_detail_id) as survei_individu_detail_id, sum(memiliki_bayi_7_sd_23_bulan) as memiliki_bayi_7_sd_23_bulan, sum(sasaran_tidak_asi_ekslusif) as sasaran_tidak_asi_ekslusif FROM asi_eksklusif where provinsi_id= ?", prov).Scan(&AsiEsk)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else {
		dbc := DbCon.Debug().Raw("SELECT count(survei_individu_detail_id) as survei_individu_detail_id, sum(memiliki_bayi_7_sd_23_bulan) as memiliki_bayi_7_sd_23_bulan, SUM(sasaran_tidak_asi_ekslusif) as sasaran_tidak_asi_ekslusif FROM asi_eksklusif").Scan(&AsiEsk)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	}
	if AsiEsk == nil {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":   "false",
			"message": "Data Kosong",
		})
	} else {
		fmt.Println(AsiEsk)
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":                              false,
			"message":                            "Data Berhasil di ambil",
			"jumlah_survei_individu_detail_id":   AsiEsk[0].Survei_individu_detail_id,
		"jumlah_memiliki_bayi_7_sd_23_bulan": AsiEsk[0].Memiliki_bayi_7_sd_23_bulan,
		"jumlah_sasaran_tidak_asi_ekslusif":  AsiEsk[0].Sasaran_tidak_asi_ekslusif,
		})
	}
}

func TestCoutAsiEsk(c echo.Context)error{
	DbCon := db.Manager()
	AsiEsk := []model.Asieksklusif{}

	dbc := DbCon.Debug().Raw("SELECT count(survei_individu_detail_id) as survei_individu_detail_id, sum(memiliki_bayi_7_sd_23_bulan) as memiliki_bayi_7_sd_23_bulan, sum(sasaran_tidak_asi_ekslusif) as sasaran_tidak_asi_ekslusif FROM public.asi_eksklusif").Scan(&AsiEsk)
	if dbc.Error != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"error":   "false",
			"message": "Kesalahan Sementara",
		})
	}
	return c.JSON(http.StatusOK, map[string]interface{}{
		"error":                              false,
		"message":                            "Data Berhasil di ambil",
		"jumlah_survei_individu_detail_id":   AsiEsk[0].Survei_individu_detail_id,
		"jumlah_memiliki_bayi_7_sd_23_bulan": AsiEsk[0].Memiliki_bayi_7_sd_23_bulan,
		"jumlah_sasaran_tidak_asi_ekslusif":  AsiEsk[0].Sasaran_tidak_asi_ekslusif,
	})
}

package api

import (
	"ApiDashboard/db"
	"ApiDashboard/model"
	"net/http"

	"github.com/labstack/echo/v4"
)

func GetWilayahKBByProgram(c echo.Context) error {
	prov := c.QueryParam("prov")
	kab := c.QueryParam("kota")
	kec := c.QueryParam("kec")
	kel := c.QueryParam("kel")
	limit := c.QueryParam("limit")
	offset := c.QueryParam("offset")
	order := c.QueryParam("order")
	DbCon := db.Manager()
	kbbp := []model.KbbyProgram{}
	if limit == "" {
		limit = "20"
	}
	if offset == "" {
		offset = "0"
	}
	if order == "" {
		order = "nama desc"
	}

	if kel != "" {
		dbc := DbCon.Debug().Where("kelurahan_id = ?", kel).Limit(limit).Offset(offset).Order(order).Find(&kbbp)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kec != "" {
		dbc := DbCon.Debug().Where("kecamatan_id = ?", kec).Limit(limit).Offset(offset).Order(order).Find(&kbbp)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kab != "" {
		dbc := DbCon.Debug().Where("kota_kabupaten_id = ?", kab).Limit(limit).Offset(offset).Order(order).Find(&kbbp)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if prov != "" {
		dbc := DbCon.Debug().Where("provinsi_id = ?", prov).Limit(limit).Offset(offset).Order(order).Find(&kbbp)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else {
		dbc := DbCon.Debug().Limit(limit).Offset(offset).Order(order).Find(&kbbp)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	}
	if kbbp == nil {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":   "false",
			"message": "Data Kosong",
		})
	} else {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":   false,
			"message": "Data Berhasil di ambil",
			"data":    kbbp,
		})
	}
}

func CountGetWilayahKBByProgram(c echo.Context) error {
	// var WANITA_KAWIN_TIDAK_HAMIL_UMUR_10_sd_14_BER_sd_KB int
	// var WANITA_KAWIN_TIDAK_HAMIL_UMUR_15_sd_49_BER_sd_KB int
	// var WANITA_KAWIN_TIDAK_HAMIL_UMUR_50_sd_54_BER_sd_KB int
	// var WANITA_KAWIN_TIDAK_HAMIL_UMUR_10_sd_14_TIDAK_BER_sd_KB int
	// var WANITA_KAWIN_TIDAK_HAMIL_UMUR_15_sd_49_TIDAK_BER_sd_KB int
	// var WANITA_KAWIN_TIDAK_HAMIL_UMUR_50_sd_54_TIDAK_BER_sd_KB int
	// var countSurveyIndividu int

	prov := c.QueryParam("prov")
	kab := c.QueryParam("kota")
	kec := c.QueryParam("kec")
	kel := c.QueryParam("kel")

	DbCon := db.Manager()
	kbbp := []model.KbbyProgram{}

	if kel != "" {
		dbc := DbCon.Debug().Raw("select sum(wanita_kawin_tidak_hamil_umur_10_sd_14_tidak_ber_sd_kb) as wanita_kawin_tidak_hamil_umur_10_sd_14_tidak_ber_sd_kb,sum(wanita_kawin_tidak_hamil_umur_15_sd_49_tidak_ber_sd_kb) as wanita_kawin_tidak_hamil_umur_15_sd_49_tidak_ber_sd_kb,sum(wanita_kawin_tidak_hamil_umur_50_sd_54_tidak_ber_sd_kb) as wanita_kawin_tidak_hamil_umur_50_sd_54_tidak_ber_sd_kb,sum(wanita_kawin_tidak_hamil_umur_10_sd_14_ber_sd_kb) as wanita_kawin_tidak_hamil_umur_10_sd_14_ber_sd_kb,sum(wanita_kawin_tidak_hamil_umur_15_sd_49_ber_sd_kb) as wanita_kawin_tidak_hamil_umur_15_sd_49_ber_sd_kb,sum(wanita_kawin_tidak_hamil_umur_50_sd_54_ber_sd_kb) as wanita_kawin_tidak_hamil_umur_50_sd_54_ber_sd_kb,count(survei_individu_detail_id) as survei_individu_detail_id from kb_by_program where kelurahan_id =?",kel).Scan(&kbbp)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kec != "" {
		dbc := DbCon.Debug().Raw("select sum(wanita_kawin_tidak_hamil_umur_10_sd_14_tidak_ber_sd_kb) as wanita_kawin_tidak_hamil_umur_10_sd_14_tidak_ber_sd_kb,sum(wanita_kawin_tidak_hamil_umur_15_sd_49_tidak_ber_sd_kb) as wanita_kawin_tidak_hamil_umur_15_sd_49_tidak_ber_sd_kb,sum(wanita_kawin_tidak_hamil_umur_50_sd_54_tidak_ber_sd_kb) as wanita_kawin_tidak_hamil_umur_50_sd_54_tidak_ber_sd_kb,sum(wanita_kawin_tidak_hamil_umur_10_sd_14_ber_sd_kb) as wanita_kawin_tidak_hamil_umur_10_sd_14_ber_sd_kb,sum(wanita_kawin_tidak_hamil_umur_15_sd_49_ber_sd_kb) as wanita_kawin_tidak_hamil_umur_15_sd_49_ber_sd_kb,sum(wanita_kawin_tidak_hamil_umur_50_sd_54_ber_sd_kb) as wanita_kawin_tidak_hamil_umur_50_sd_54_ber_sd_kb,count(survei_individu_detail_id) as survei_individu_detail_id from kb_by_program where kecamatan_id =?",kec).Scan(&kbbp)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kab != "" {
		dbc := DbCon.Debug().Raw("select sum(wanita_kawin_tidak_hamil_umur_10_sd_14_tidak_ber_sd_kb) as wanita_kawin_tidak_hamil_umur_10_sd_14_tidak_ber_sd_kb,sum(wanita_kawin_tidak_hamil_umur_15_sd_49_tidak_ber_sd_kb) as wanita_kawin_tidak_hamil_umur_15_sd_49_tidak_ber_sd_kb,sum(wanita_kawin_tidak_hamil_umur_50_sd_54_tidak_ber_sd_kb) as wanita_kawin_tidak_hamil_umur_50_sd_54_tidak_ber_sd_kb,sum(wanita_kawin_tidak_hamil_umur_10_sd_14_ber_sd_kb) as wanita_kawin_tidak_hamil_umur_10_sd_14_ber_sd_kb,sum(wanita_kawin_tidak_hamil_umur_15_sd_49_ber_sd_kb) as wanita_kawin_tidak_hamil_umur_15_sd_49_ber_sd_kb,sum(wanita_kawin_tidak_hamil_umur_50_sd_54_ber_sd_kb) as wanita_kawin_tidak_hamil_umur_50_sd_54_ber_sd_kb,count(survei_individu_detail_id) as survei_individu_detail_id from kb_by_program where kota_kabupaten_id =?",kab).Scan(&kbbp)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if prov != "" {
		dbc := DbCon.Debug().Raw("select sum(wanita_kawin_tidak_hamil_umur_10_sd_14_tidak_ber_sd_kb) as wanita_kawin_tidak_hamil_umur_10_sd_14_tidak_ber_sd_kb,sum(wanita_kawin_tidak_hamil_umur_15_sd_49_tidak_ber_sd_kb) as wanita_kawin_tidak_hamil_umur_15_sd_49_tidak_ber_sd_kb,sum(wanita_kawin_tidak_hamil_umur_50_sd_54_tidak_ber_sd_kb) as wanita_kawin_tidak_hamil_umur_50_sd_54_tidak_ber_sd_kb,sum(wanita_kawin_tidak_hamil_umur_10_sd_14_ber_sd_kb) as wanita_kawin_tidak_hamil_umur_10_sd_14_ber_sd_kb,sum(wanita_kawin_tidak_hamil_umur_15_sd_49_ber_sd_kb) as wanita_kawin_tidak_hamil_umur_15_sd_49_ber_sd_kb,sum(wanita_kawin_tidak_hamil_umur_50_sd_54_ber_sd_kb) as wanita_kawin_tidak_hamil_umur_50_sd_54_ber_sd_kb,count(survei_individu_detail_id) as survei_individu_detail_id from kb_by_program where provinsi_id =?",prov).Scan(&kbbp)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else {
		dbc := DbCon.Debug().Raw("select sum(wanita_kawin_tidak_hamil_umur_10_sd_14_tidak_ber_sd_kb) as wanita_kawin_tidak_hamil_umur_10_sd_14_tidak_ber_sd_kb,sum(wanita_kawin_tidak_hamil_umur_15_sd_49_tidak_ber_sd_kb) as wanita_kawin_tidak_hamil_umur_15_sd_49_tidak_ber_sd_kb,sum(wanita_kawin_tidak_hamil_umur_50_sd_54_tidak_ber_sd_kb) as wanita_kawin_tidak_hamil_umur_50_sd_54_tidak_ber_sd_kb,sum(wanita_kawin_tidak_hamil_umur_10_sd_14_ber_sd_kb) as wanita_kawin_tidak_hamil_umur_10_sd_14_ber_sd_kb,sum(wanita_kawin_tidak_hamil_umur_15_sd_49_ber_sd_kb) as wanita_kawin_tidak_hamil_umur_15_sd_49_ber_sd_kb,sum(wanita_kawin_tidak_hamil_umur_50_sd_54_ber_sd_kb) as wanita_kawin_tidak_hamil_umur_50_sd_54_ber_sd_kb,count(survei_individu_detail_id) as survei_individu_detail_id from kb_by_program").Scan(&kbbp)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	}
	if kbbp == nil {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":   "false",
			"message": "Data Kosong",
		})
	} else {
		

		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":                            false,
			"message":                          "Data Berhasil di ambil",
			"jumlah_survei_individu_detail_id": kbbp[0].Survei_individu_detail_id,
			"wanita_kawin_tidak_hamil_umur_10_sd_14_ber_sd_kb":       kbbp[0].Wanita_kawin_tidak_hamil_umur_10_sd_14_ber_sd_kb,
			"wanita_kawin_tidak_hamil_umur_15_sd_49_ber_sd_kb":       kbbp[0].Wanita_kawin_tidak_hamil_umur_15_sd_49_ber_sd_kb,
			"wanita_kawin_tidak_hamil_umur_50_sd_54_ber_sd_kb":       kbbp[0].Wanita_kawin_tidak_hamil_umur_50_sd_54_ber_sd_kb,
			"wanita_kawin_tidak_hamil_umur_10_sd_14_tidak_ber_sd_kb": kbbp[0].Wanita_kawin_tidak_hamil_umur_10_sd_14_tidak_ber_sd_kb,
			"wanita_kawin_tidak_hamil_umur_15_sd_49_tidak_ber_sd_kb": kbbp[0].Wanita_kawin_tidak_hamil_umur_15_sd_49_tidak_ber_sd_kb,
			"wanita_kawin_tidak_hamil_umur_50_sd_54_tidak_ber_sd_kb": kbbp[0].Wanita_kawin_tidak_hamil_umur_50_sd_54_tidak_ber_sd_kb,
		})
	}
}

package api

import (
	"ApiDashboard/db"
	"ApiDashboard/model"
	"net/http"

	"github.com/labstack/echo/v4"
)

func GetWilayahKB(c echo.Context) error {
	prov := c.QueryParam("prov")
	kab := c.QueryParam("kota")
	kec := c.QueryParam("kec")
	kel := c.QueryParam("kel")
	limit := c.QueryParam("limit")
	offset := c.QueryParam("offset")
	order := c.QueryParam("order")
	DbCon := db.Manager()
	kb := []model.KeluargaBerencana{}
	if limit == "" {
		limit = "20"
	}
	if offset == "" {
		offset = "0"
	}
	if order == "" {
		order = "nama desc"
	}

	if kel != "" {
		dbc := DbCon.Debug().Where("kelurahan_id = ?", kel).Limit(limit).Offset(offset).Order(order).Find(&kb)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kec != "" {
		dbc := DbCon.Debug().Where("kecamatan_id = ?", kec).Limit(limit).Offset(offset).Order(order).Find(&kb)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kab != "" {
		dbc := DbCon.Debug().Where("kota_kabupaten_id = ?", kab).Limit(limit).Offset(offset).Order(order).Find(&kb)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if prov != "" {
		dbc := DbCon.Debug().Where("provinsi_id = ?", prov).Limit(limit).Offset(offset).Order(order).Find(&kb)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else {
		dbc := DbCon.Debug().Limit(limit).Offset(offset).Order(order).Find(&kb)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	}
	if kb == nil {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":   "false",
			"message": "Data Kosong",
		})
	} else {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":   false,
			"message": "Data Berhasil di ambil",
			"data":    kb,
		})
	}
}

func CountGetWilayahKB(c echo.Context) error {
	prov := c.QueryParam("prov")
	kab := c.QueryParam("kota")
	kec := c.QueryParam("kec")
	kel := c.QueryParam("kel")
	DbCon := db.Manager()
	kbbp := []model.KeluargaBerencana{}

	// var USIA_lebih_dari_10_TAHUN int
	// var USIA_10_sd_54 int
	// var WANITA_USIA_10_sd_54_KAWIN int
	// var PRIA_USIA_lebih_dari_10_TAHUN_KAWIN int
	// var WANITA_USIA_10_sd_54_DAN_PRIA_USIA_lebih_10_SUDAH_KAWIN int
	// var WANITA_USIA_10_sd_54_SUDAH_KAWIN_TIDAK_HAMIL int
	// var WANITA_USIA_10_sd_54_SUDAH_KAWIN_TIDAK_HAMIL_BER_KB int
	// var Wanita_usia_10_sd_54_sudahkawin_tidak_hamil_tidak_berkb int
	// var WANITA_BER_KB int
	// var WANITA_TIDAK_BER_KB int
	// var countSurveyIndividu int
	if kel != "" {
		dbc := DbCon.Debug().Raw("select count(survei_individu_detail_id) as survei_individu_detail_id,sum(wanita_tidak_ber_kb) as wanita_tidak_ber_kb,sum(wanita_ber_kb) as wanita_ber_kb,sum(wanita_usia_10_sd_54_sudahkawin_tidak_hamil_tidak_berkb) as wanita_usia_10_sd_54_sudahkawin_tidak_hamil_tidak_berkb,sum(wanita_usia_10_sd_54_sudah_kawin_tidak_hamil_ber_kb) as wanita_usia_10_sd_54_sudah_kawin_tidak_hamil_ber_kb,sum(wanita_usia_10_sd_54_sudah_kawin_tidak_hamil) as wanita_usia_10_sd_54_sudah_kawin_tidak_hamil,sum(wanita_usia_10_sd_54_dan_pria_usia_lebih_10_sudah_kawin) as wanita_usia_10_sd_54_dan_pria_usia_lebih_10_sudah_kawin,sum(pria_usia_lebih_dari_10_tahun_kawin) as pria_usia_lebih_dari_10_tahun_kawin,sum(wanita_usia_10_sd_54_kawin) as wanita_usia_10_sd_54_kawin,sum(usia_10_sd_54) as usia_10_sd_54,sum(usia_lebih_dari_10_tahun) as usia_lebih_dari_10_tahun from keluarga_berencana where kelurahan_id=?", kel).Scan(&kbbp)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kec != "" {
		dbc := DbCon.Debug().Raw("select count(survei_individu_detail_id) as survei_individu_detail_id,sum(wanita_tidak_ber_kb) as wanita_tidak_ber_kb,sum(wanita_ber_kb) as wanita_ber_kb,sum(wanita_usia_10_sd_54_sudahkawin_tidak_hamil_tidak_berkb) as wanita_usia_10_sd_54_sudahkawin_tidak_hamil_tidak_berkb,sum(wanita_usia_10_sd_54_sudah_kawin_tidak_hamil_ber_kb) as wanita_usia_10_sd_54_sudah_kawin_tidak_hamil_ber_kb,sum(wanita_usia_10_sd_54_sudah_kawin_tidak_hamil) as wanita_usia_10_sd_54_sudah_kawin_tidak_hamil,sum(wanita_usia_10_sd_54_dan_pria_usia_lebih_10_sudah_kawin) as wanita_usia_10_sd_54_dan_pria_usia_lebih_10_sudah_kawin,sum(pria_usia_lebih_dari_10_tahun_kawin) as pria_usia_lebih_dari_10_tahun_kawin,sum(wanita_usia_10_sd_54_kawin) as wanita_usia_10_sd_54_kawin,sum(usia_10_sd_54) as usia_10_sd_54,sum(usia_lebih_dari_10_tahun) as usia_lebih_dari_10_tahun from keluarga_berencana where kecamatan_id=?", kec).Scan(&kbbp)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kab != "" {
		dbc := DbCon.Debug().Raw("select count(survei_individu_detail_id) as survei_individu_detail_id,sum(wanita_tidak_ber_kb) as wanita_tidak_ber_kb,sum(wanita_ber_kb) as wanita_ber_kb,sum(wanita_usia_10_sd_54_sudahkawin_tidak_hamil_tidak_berkb) as wanita_usia_10_sd_54_sudahkawin_tidak_hamil_tidak_berkb,sum(wanita_usia_10_sd_54_sudah_kawin_tidak_hamil_ber_kb) as wanita_usia_10_sd_54_sudah_kawin_tidak_hamil_ber_kb,sum(wanita_usia_10_sd_54_sudah_kawin_tidak_hamil) as wanita_usia_10_sd_54_sudah_kawin_tidak_hamil,sum(wanita_usia_10_sd_54_dan_pria_usia_lebih_10_sudah_kawin) as wanita_usia_10_sd_54_dan_pria_usia_lebih_10_sudah_kawin,sum(pria_usia_lebih_dari_10_tahun_kawin) as pria_usia_lebih_dari_10_tahun_kawin,sum(wanita_usia_10_sd_54_kawin) as wanita_usia_10_sd_54_kawin,sum(usia_10_sd_54) as usia_10_sd_54,sum(usia_lebih_dari_10_tahun) as usia_lebih_dari_10_tahun from keluarga_berencana where kot_kabupaten_id=?", kab).Scan(&kbbp)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if prov != "" {
		dbc := DbCon.Debug().Raw("select count(survei_individu_detail_id) as survei_individu_detail_id,sum(wanita_tidak_ber_kb) as wanita_tidak_ber_kb,sum(wanita_ber_kb) as wanita_ber_kb,sum(wanita_usia_10_sd_54_sudahkawin_tidak_hamil_tidak_berkb) as wanita_usia_10_sd_54_sudahkawin_tidak_hamil_tidak_berkb,sum(wanita_usia_10_sd_54_sudah_kawin_tidak_hamil_ber_kb) as wanita_usia_10_sd_54_sudah_kawin_tidak_hamil_ber_kb,sum(wanita_usia_10_sd_54_sudah_kawin_tidak_hamil) as wanita_usia_10_sd_54_sudah_kawin_tidak_hamil,sum(wanita_usia_10_sd_54_dan_pria_usia_lebih_10_sudah_kawin) as wanita_usia_10_sd_54_dan_pria_usia_lebih_10_sudah_kawin,sum(pria_usia_lebih_dari_10_tahun_kawin) as pria_usia_lebih_dari_10_tahun_kawin,sum(wanita_usia_10_sd_54_kawin) as wanita_usia_10_sd_54_kawin,sum(usia_10_sd_54) as usia_10_sd_54,sum(usia_lebih_dari_10_tahun) as usia_lebih_dari_10_tahun from keluarga_berencana where provinsi_id=?", prov).Scan(&kbbp)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else {
		//select count and sum
		dbc := DbCon.Debug().Raw("select count(survei_individu_detail_id) as survei_individu_detail_id,sum(wanita_tidak_ber_kb) as wanita_tidak_ber_kb,sum(wanita_ber_kb) as wanita_ber_kb,sum(wanita_usia_10_sd_54_sudahkawin_tidak_hamil_tidak_berkb) as wanita_usia_10_sd_54_sudahkawin_tidak_hamil_tidak_berkb,sum(wanita_usia_10_sd_54_sudah_kawin_tidak_hamil_ber_kb) as wanita_usia_10_sd_54_sudah_kawin_tidak_hamil_ber_kb,sum(wanita_usia_10_sd_54_sudah_kawin_tidak_hamil) as wanita_usia_10_sd_54_sudah_kawin_tidak_hamil,sum(wanita_usia_10_sd_54_dan_pria_usia_lebih_10_sudah_kawin) as wanita_usia_10_sd_54_dan_pria_usia_lebih_10_sudah_kawin,sum(pria_usia_lebih_dari_10_tahun_kawin) as pria_usia_lebih_dari_10_tahun_kawin,sum(wanita_usia_10_sd_54_kawin) as wanita_usia_10_sd_54_kawin,sum(usia_10_sd_54) as usia_10_sd_54,sum(usia_lebih_dari_10_tahun) as usia_lebih_dari_10_tahun from keluarga_berencana").Scan(&kbbp)
		// dbc := DbCon.Debug().Select("survei_individu_detail_id,wanita_tidak_ber_kb,wanita_ber_kb,wanita_usia_10_sd_54_sudahkawin_tidak_hamil_tidak_berkb,wanita_usia_10_sd_54_sudah_kawin_tidak_hamil_ber_kb,wanita_usia_10_sd_54_sudah_kawin_tidak_hamil,wanita_usia_10_sd_54_dan_pria_usia_lebih_10_sudah_kawin,pria_usia_lebih_dari_10_tahun_kawin,wanita_usia_10_sd_54_kawin,usia_10_sd_54,usia_lebih_dari_10_tahun").Find(&kbbp)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	}
	if kbbp == nil {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":   "false",
			"message": "Data Kosong",
		})
	} else {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":                                      false,
			"message":                                    "Data Berhasil di ambil",
			"jumlah_survei_individu_detail_id":           kbbp[0].Survei_individu_detail_id,
			"jumlah_usia_lebih_dari_10_tahun":            kbbp[0].Usia_lebih_dari_10_tahun,
			"jumlah_usia_10_sd_54":                       kbbp[0].Usia_10_sd_54,
			"jumlah_wanita_usia_10_sd_54_kawin":          kbbp[0].Wanita_usia_10_sd_54_kawin,
			"jumlah_pria_usia_lebih_dari_10_tahun_kawin": kbbp[0].Pria_usia_lebih_dari_10_tahun_kawin,
			"jumlah_wanita_usia_10_sd_54_dan_pria_usia_lebih_10_sudah_kawin":  kbbp[0].Wanita_usia_10_sd_54_dan_pria_usia_lebih_10_sudah_kawin,
			"jumlah_wanita_usia_10_sd_54_sudah_kawin_tidak_hamil":             kbbp[0].Wanita_usia_10_sd_54_sudah_kawin_tidak_hamil,
			"jumlah_wanita_usia_10_sd_54_sudah_kawin_tidak_hamil_ber_kb":      kbbp[0].Wanita_usia_10_sd_54_sudah_kawin_tidak_hamil_ber_kb,
			"jumlah_wanita_usia_10_sd_54_sudah_kawin_tidak_hamil_tidak_berkb": kbbp[0].Wanita_usia_10_sd_54_sudahkawin_tidak_hamil_tidak_berkb,
			"jumlah_wanita_ber_kb":       kbbp[0].Wanita_ber_kb,
			"jumlah_wanita_tidak_ber_kb": kbbp[0].Wanita_tidak_ber_kb,
		})
	}
}
package api

import (
	"ApiDashboard/db"
	"ApiDashboard/model"
	"net/http"

	"github.com/labstack/echo/v4"
)

func GetWilayahRokokbyProgram(c echo.Context) error {
	prov := c.QueryParam("prov")
	kab := c.QueryParam("kota")
	kec := c.QueryParam("kec")
	kel := c.QueryParam("kel")
	limit := c.QueryParam("limit")
	offset := c.QueryParam("offset")
	order := c.QueryParam("order")
	DbCon := db.Manager()
	rkbp := []model.Rokokbyprogram{}
	if limit == "" {
		limit = "20"
	}
	if offset == "" {
		offset = "0"
	}
	if order == "" {
		order = "nama desc"
	}

	if kel != "" {
		dbc := DbCon.Debug().Where("kelurahan_id = ?", kel).Limit(limit).Offset(offset).Order(order).Find(&rkbp)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kec != "" {
		dbc := DbCon.Debug().Where("kecamatan_id = ?", kec).Limit(limit).Offset(offset).Order(order).Find(&rkbp)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kab != "" {
		dbc := DbCon.Debug().Where("kota_kabupaten_id = ?", kab).Limit(limit).Offset(offset).Order(order).Find(&rkbp)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if prov != "" {
		dbc := DbCon.Debug().Where("provinsi_id = ?", prov).Limit(limit).Offset(offset).Order(order).Find(&rkbp)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else {
		dbc := DbCon.Debug().Limit(limit).Offset(offset).Order(order).Find(&rkbp)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	}
	if rkbp == nil {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":   "false",
			"message": "Data Kosong",
		})
	} else {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":   false,
			"message": "Data Berhasil di ambil",
			"data":    rkbp,
		})
	}
}

func CountGetWilayahRokokbyProgram(c echo.Context) error {
	// var Survei_individu_detail_id int
	// var Perokok_umur_10_sd_18_tahun int
	// var Perokok_umur_15_sd_18_tahun int
	// var Perokok_umur_diatas_15_tahun int

	prov := c.QueryParam("prov")
	kab := c.QueryParam("kota")
	kec := c.QueryParam("kec")
	kel := c.QueryParam("kel")

	DbCon := db.Manager()
	rkbp := []model.Rokokbyprogram{}

	if kel != "" {
		dbc := DbCon.Debug().Raw("select 	count(survei_individu_detail_id) as survei_individu_detail_id,	sum(perokok_umur_10_sd_18_tahun) as perokok_umur_10_sd_18_tahun,	sum(perokok_umur_15_sd_18_tahun) as perokok_umur_15_sd_18_tahun,	sum(perokok_umur_diatas_15_tahun) as perokok_umur_diatas_15_tahun	from rokok_by_program where kelurahan_id = ?", kel).Scan(&rkbp)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kec != "" {
		dbc := DbCon.Debug().Raw("select 	count(survei_individu_detail_id) as survei_individu_detail_id,	sum(perokok_umur_10_sd_18_tahun) as perokok_umur_10_sd_18_tahun,	sum(perokok_umur_15_sd_18_tahun) as perokok_umur_15_sd_18_tahun,	sum(perokok_umur_diatas_15_tahun) as perokok_umur_diatas_15_tahun	from rokok_by_program where kecamatan_id = ?", kec).Scan(&rkbp)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kab != "" {
		dbc := DbCon.Debug().Raw("select 	count(survei_individu_detail_id) as survei_individu_detail_id,	sum(perokok_umur_10_sd_18_tahun) as perokok_umur_10_sd_18_tahun,	sum(perokok_umur_15_sd_18_tahun) as perokok_umur_15_sd_18_tahun,	sum(perokok_umur_diatas_15_tahun) as perokok_umur_diatas_15_tahun	from rokok_by_program where kota_kabupaten_id = ?", kab).Scan(&rkbp)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if prov != "" {
		dbc := DbCon.Debug().Raw("select count(survei_individu_detail_id) as survei_individu_detail_id,		sum(perokok_umur_10_sd_18_tahun) as perokok_umur_10_sd_18_tahun,		sum(perokok_umur_15_sd_18_tahun) as perokok_umur_15_sd_18_tahun,		sum(perokok_umur_diatas_15_tahun) as perokok_umur_diatas_15_tahun		from rokok_by_program where provinsi_id = ?", prov).Scan(&rkbp)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else {
		dbc := DbCon.Debug().Raw("select count(survei_individu_detail_id) as survei_individu_detail_id,sum(perokok_umur_10_sd_18_tahun) as perokok_umur_10_sd_18_tahun,sum(perokok_umur_15_sd_18_tahun) as perokok_umur_15_sd_18_tahun,sum(perokok_umur_diatas_15_tahun) as perokok_umur_diatas_15_tahun from rokok_by_program").Scan(&rkbp)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	}
	if rkbp == nil {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":   "false",
			"message": "Data Kosong",
		})
	} else {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":                               false,
			"message":                             "Data Berhasil di ambil",
			"jumlah_survei_individu_detail_id":    rkbp[0].Survei_individu_detail_id,
			"jumlah_perokok_umur_10_sd_18_tahun":  rkbp[0].Perokok_umur_10_sd_18_tahun,
			"jumlah_perokok_umur_15_sd_18_tahun":  rkbp[0].Perokok_umur_15_sd_18_tahun,
			"jumlah_perokok_umur_diatas_15_tahun": rkbp[0].Perokok_umur_diatas_15_tahun,
		})
	}
}

package api

import (
	"ApiDashboard/db"
	"ApiDashboard/model"
	"net/http"

	"github.com/labstack/echo/v4"
)

func GetWilayahKarakterRes(c echo.Context) error {
	prov := c.QueryParam("prov")
	kab := c.QueryParam("kota")
	kec := c.QueryParam("kec")
	kel := c.QueryParam("kel")
	limit := c.QueryParam("limit")
	offset := c.QueryParam("offset")
	order := c.QueryParam("order")
	DbCon := db.Manager()
	KarakterRes := []model.KarateristikResponden{}
	if limit == "" {
		limit = "20"
	}
	if offset == "" {
		offset = "0"
	}
	if order == "" {
		order = "nama desc"
	}

	if kel != "" {
		dbc := DbCon.Debug().Where("kelurahan_id = ?", kel).Limit(limit).Offset(offset).Order(order).Find(&KarakterRes)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kec != "" {
		dbc := DbCon.Debug().Where("kecamatan_id = ?", kec).Limit(limit).Offset(offset).Order(order).Find(&KarakterRes)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kab != "" {
		dbc := DbCon.Debug().Where("kota_kabupaten_id = ?", kab).Limit(limit).Offset(offset).Order(order).Find(&KarakterRes)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if prov != "" {
		dbc := DbCon.Debug().Where("provinsi_id = ?", prov).Limit(limit).Offset(offset).Order(order).Find(&KarakterRes)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else {
		dbc := DbCon.Debug().Limit(limit).Offset(offset).Order(order).Find(&KarakterRes)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	}
	if KarakterRes == nil {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":   "false",
			"message": "Data Kosong",
		})
	} else {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":   false,
			"message": "Data Berhasil di ambil",
			"data":    KarakterRes,
		})
	}
}
func CountGetWilayahKarakterRes(c echo.Context) error {
	prov := c.QueryParam("prov")
	kab := c.QueryParam("kota")
	kec := c.QueryParam("kec")
	kel := c.QueryParam("kel")

	DbCon := db.Manager()
	KarakterRes := []model.KarateristikResponden{}

	if kel != "" {
		dbc := DbCon.Debug().Raw("SELECT count(survei_individu_detail_id) as survei_individu_detail_id, sum(umur_5_sd_9_tahun) as umur_5_sd_9_tahun, sum(umur_10_sd_14_tahun) as umur_10_sd_14_tahun, sum(umur_15_24_tahun) as umur_15_24_tahun, sum(umur_25_34_tahun) as umur_25_34_tahun, sum(umur_35_44_tahun) as umur_35_44_tahun, sum(umur_45_54_tahun) as umur_45_54_tahun, sum(umur_55_64_tahun) as umur_55_64_tahun, sum(umur_65_tahun_keatas) as umur_65_tahun_keatas FROM karakteristik_responden where kelurahan_id =?", kel).Find(&KarakterRes)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kec != "" {
		dbc := DbCon.Debug().Raw("SELECT count(survei_individu_detail_id) as survei_individu_detail_id, sum(umur_5_sd_9_tahun) as umur_5_sd_9_tahun, sum(umur_10_sd_14_tahun) as umur_10_sd_14_tahun, sum(umur_15_24_tahun) as umur_15_24_tahun, sum(umur_25_34_tahun) as umur_25_34_tahun, sum(umur_35_44_tahun) as umur_35_44_tahun, sum(umur_45_54_tahun) as umur_45_54_tahun, sum(umur_55_64_tahun) as umur_55_64_tahun, sum(umur_65_tahun_keatas) as umur_65_tahun_keatas FROM karakteristik_responden where kecamatan_id = ?", kec).Find(&KarakterRes)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kab != "" {
		dbc := DbCon.Debug().Raw("SELECT count(survei_individu_detail_id) as survei_individu_detail_id, sum(umur_5_sd_9_tahun) as umur_5_sd_9_tahun, sum(umur_10_sd_14_tahun) as umur_10_sd_14_tahun, sum(umur_15_24_tahun) as umur_15_24_tahun, sum(umur_25_34_tahun) as umur_25_34_tahun, sum(umur_35_44_tahun) as umur_35_44_tahun, sum(umur_45_54_tahun) as umur_45_54_tahun, sum(umur_55_64_tahun) as umur_55_64_tahun, sum(umur_65_tahun_keatas) as umur_65_tahun_keatas FROM karakteristik_responden where kota_kabupaten_id = ?", kab).Find(&KarakterRes)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if prov != "" {
		dbc := DbCon.Debug().Raw("SELECT count(survei_individu_detail_id) as survei_individu_detail_id, sum(umur_5_sd_9_tahun) as umur_5_sd_9_tahun, sum(umur_10_sd_14_tahun) as umur_10_sd_14_tahun, sum(umur_15_24_tahun) as umur_15_24_tahun, sum(umur_25_34_tahun) as umur_25_34_tahun, sum(umur_35_44_tahun) as umur_35_44_tahun, sum(umur_45_54_tahun) as umur_45_54_tahun, sum(umur_55_64_tahun) as umur_55_64_tahun, sum(umur_65_tahun_keatas) as umur_65_tahun_keatas FROM karakteristik_responden where provinsi_id = ?", prov).Find(&KarakterRes)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else {
		dbc := DbCon.Debug().Raw("SELECT count(survei_individu_detail_id) as survei_individu_detail_id, sum(umur_5_sd_9_tahun) as umur_5_sd_9_tahun, sum(umur_10_sd_14_tahun) as umur_10_sd_14_tahun, sum(umur_15_24_tahun) as umur_15_24_tahun, sum(umur_25_34_tahun) as umur_25_34_tahun, sum(umur_35_44_tahun) as umur_35_44_tahun, sum(umur_45_54_tahun) as umur_45_54_tahun, sum(umur_55_64_tahun) as umur_55_64_tahun, sum(umur_65_tahun_keatas) as umur_65_tahun_keatas FROM karakteristik_responden").Find(&KarakterRes)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	}

	if KarakterRes == nil {
		return c.JSON(http.StatusNoContent, map[string]interface{}{
			"error":   "false",
			"message": "Data Kosong",
		})
	} else {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":                            false,
			"message":                          "Data Berhasil di ambil",
			"jumlah_umur_5_sd_9_tahun":         KarakterRes[0].Umur_5_sd_9_tahun,
			"jumlah_umur_10_sd_14_tahun":       KarakterRes[0].Umur_10_sd_14_tahun,
			"jumlah_umur_15_24_tahun":          KarakterRes[0].Umur_15_24_tahun,
			"jumlah_umur_25_34_tahun":          KarakterRes[0].Umur_25_34_tahun,
			"jumlah_umur_35_44_tahun":          KarakterRes[0].Umur_35_44_tahun,
			"jumlah_umur_45_54_tahun":          KarakterRes[0].Umur_45_54_tahun,
			"jumlah_umur_55_64_tahun":          KarakterRes[0].Umur_55_64_tahun,
			"jumlah_umur_65_tahun_keatas":      KarakterRes[0].Umur_65_tahun_keatas,
			"jumlah_survei_individu_detail_id": KarakterRes[0].Survei_individu_detail_id,
		})
	}
}

func TesCoutKarakterRes(c echo.Context) error {
	DbCon := db.Manager()
	KarakterRes := []model.KarateristikResponden{}
	DbCon.Debug().Raw("SELECT count(survei_individu_detail_id) as survei_individu_detail_id, sum(umur_5_sd_9_tahun) as umur_5_sd_9_tahun, sum(umur_10_sd_14_tahun) as umur_10_sd_14_tahun, sum(umur_15_24_tahun) as umur_15_24_tahun, sum(umur_25_34_tahun) as umur_25_34_tahun, sum(umur_35_44_tahun) as umur_35_44_tahun, sum(umur_45_54_tahun) as umur_45_54_tahun, sum(umur_55_64_tahun) as umur_55_64_tahun, sum(umur_65_tahun_keatas) as umur_65_tahun_keatas FROM karakteristik_responden").Find(&KarakterRes)
	if DbCon.Error != nil {
		return c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"error":   "true",
			"message": "Kesalahan Sementara",
		})
	}
	return c.JSON(http.StatusOK, map[string]interface{}{
		"error":                            false,
		"message":                          "Data Berhasil di ambil",
		"jumlah_umur_5_sd_9_tahun":         KarakterRes[0].Umur_5_sd_9_tahun,
		"jumlah_umur_10_sd_14_tahun":       KarakterRes[0].Umur_10_sd_14_tahun,
		"jumlah_umur_15_24_tahun":          KarakterRes[0].Umur_15_24_tahun,
		"jumlah_umur_25_34_tahun":          KarakterRes[0].Umur_25_34_tahun,
		"jumlah_umur_35_44_tahun":          KarakterRes[0].Umur_35_44_tahun,
		"jumlah_umur_45_54_tahun":          KarakterRes[0].Umur_45_54_tahun,
		"jumlah_umur_55_64_tahun":          KarakterRes[0].Umur_55_64_tahun,
		"jumlah_umur_65_tahun_keatas":      KarakterRes[0].Umur_65_tahun_keatas,
		"jumlah_survei_individu_detail_id": KarakterRes[0].Survei_individu_detail_id,
	})
}

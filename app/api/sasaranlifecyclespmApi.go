package api

import (
	"ApiDashboard/db"
	"ApiDashboard/model"
	"net/http"

	// "time"

	"github.com/labstack/echo/v4"
)

type ReturnJson struct {
	Error   bool   `json:"error"`
	Message string `json:"message"`
	Data    interface{}
}

//getdatabywilayahrcho
//sasaranlifecyclespm
func GetWilayahSpm(c echo.Context) error {
	prov := c.QueryParam("prov")
	kab := c.QueryParam("kota")
	kec := c.QueryParam("kec")
	kel := c.QueryParam("kel")
	limit := c.QueryParam("limit")
	offset := c.QueryParam("offset")
	order := c.QueryParam("order")
	DbCon := db.Manager()
	spm := []model.SasaranLifecycleSpm{}
	if limit == "" {
		limit = "20"
	}
	if offset == "" {
		offset = "0"
	}
	if order == "" {
		order = "nama desc"
	}
	if kel != "" {
		dbc := DbCon.Debug().Where("kelurahan_id = ?", kel).Limit(limit).Offset(offset).Order(order).Find(&spm)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kec != "" {
		dbc := DbCon.Debug().Where("kecamatan_id = ?", kec).Limit(limit).Offset(offset).Order(order).Find(&spm)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kab != "" {
		dbc := DbCon.Debug().Where("kota_kabupaten_id = ?", kab).Limit(limit).Offset(offset).Order(order).Find(&spm)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if prov != "" {
		dbc := DbCon.Debug().Where("provinsi_id = ?", prov).Limit(limit).Offset(offset).Order(order).Find(&spm)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else {
		dbc := DbCon.Debug().Limit(limit).Offset(offset).Order(order).Find(&spm)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	}
	if spm == nil {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":   "false",
			"message": "Data Kosong",
		})
	} else {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":   false,
			"message": "Data Berhasil di ambil",
			"data":    spm,
		})
	}
}

func CountGetWilayahSpm(c echo.Context) error {
	// var Survei_individu_detail_id int
	// var HAMIL int
	// var USIA_0_sd_59_BULAN int
	// var USIA_7_sd_15_TAHUN int
	// var USIA_15_sd_59_TAHUN int
	// var Usia_lbh_dari_60_tahun int
	prov := c.QueryParam("prov")
	kab := c.QueryParam("kota")
	kec := c.QueryParam("kec")
	kel := c.QueryParam("kel")

	DbCon := db.Manager()
	spm := []model.SasaranLifecycleSpm{}

	if kel != "" {
		dbc := DbCon.Debug().Raw("select count(survei_individu_detail_id) as survei_individu_detail_id,sum(hamil) as hamil, sum(usia_0_sd_59_bulan) as usia_0_sd_59_bulan,sum(usia_7_sd_15_tahun) as usia_7_sd_15_tahun,sum(usia_15_sd_59_tahun) as usia_15_sd_59_tahun,sum(usia_lbh_dari_60_tahun) as usia_lbh_dari_60_tahun from sasaran_life_cycle_spm where kelurahan_id=?",kel).Scan(&spm)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kec != "" {
		dbc := DbCon.Debug().Raw("select count(survei_individu_detail_id) as survei_individu_detail_id,sum(hamil) as hamil, sum(usia_0_sd_59_bulan) as usia_0_sd_59_bulan,sum(usia_7_sd_15_tahun) as usia_7_sd_15_tahun,sum(usia_15_sd_59_tahun) as usia_15_sd_59_tahun,sum(usia_lbh_dari_60_tahun) as usia_lbh_dari_60_tahun from sasaran_life_cycle_spm where kecamatan_id=?",kec).Scan(&spm)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kab != "" {
		dbc := DbCon.Debug().Raw("select count(survei_individu_detail_id) as survei_individu_detail_id,sum(hamil) as hamil, sum(usia_0_sd_59_bulan) as usia_0_sd_59_bulan,sum(usia_7_sd_15_tahun) as usia_7_sd_15_tahun,sum(usia_15_sd_59_tahun) as usia_15_sd_59_tahun,sum(usia_lbh_dari_60_tahun) as usia_lbh_dari_60_tahun from sasaran_life_cycle_spm where kota_kabupaten_id=?",kab).Scan(&spm)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if prov != "" {
		dbc := DbCon.Debug().Raw("select count(survei_individu_detail_id) as survei_individu_detail_id,sum(hamil) as hamil, sum(usia_0_sd_59_bulan) as usia_0_sd_59_bulan,sum(usia_7_sd_15_tahun) as usia_7_sd_15_tahun,sum(usia_15_sd_59_tahun) as usia_15_sd_59_tahun,sum(usia_lbh_dari_60_tahun) as usia_lbh_dari_60_tahun from sasaran_life_cycle_spm where provinsi_id=?",prov).Scan(&spm)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else {
		dbc := DbCon.Debug().Raw("select count(survei_individu_detail_id) as survei_individu_detail_id,sum(hamil) as hamil, sum(usia_0_sd_59_bulan) as usia_0_sd_59_bulan,sum(usia_7_sd_15_tahun) as usia_7_sd_15_tahun,sum(usia_15_sd_59_tahun) as usia_15_sd_59_tahun,sum(usia_lbh_dari_60_tahun) as usia_lbh_dari_60_tahun from sasaran_life_cycle_spm").Scan(&spm)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	}
	if spm == nil {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":   "false",
			"message": "Data Kosong",
		})
	} else {
		

		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":                            false,
			"message":                          "Data Berhasil di ambil",
			"jumlah_Survei_individu_detail_id": spm[0].Survei_individu_detail_id,
			"jumlah_hamil":                     spm[0].Hamil,
			"jumlah_usia_0_sd_59_bulan":        spm[0].Usia_0_sd_59_bulan,
			"jumlah_usia_7_sd_15_tahun":        spm[0].Usia_7_sd_15_tahun,
			"jumlah_usia_15_sd_59_tahun":       spm[0].Usia_15_sd_59_tahun,
			"jumlah_usia_lbh_dari_60_tahun":    spm[0].Usia_lbh_dari_60_tahun,
		})
	}
}

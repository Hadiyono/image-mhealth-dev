package api

import (
	"ApiDashboard/db"
	"ApiDashboard/model"
	"net/http"

	"github.com/labstack/echo/v4"
)

func GetWilayahJKN(c echo.Context) error {
	prov := c.QueryParam("prov")
	kab := c.QueryParam("kota")
	kec := c.QueryParam("kec")
	kel := c.QueryParam("kel")
	limit := c.QueryParam("limit")
	offset := c.QueryParam("offset")
	order := c.QueryParam("order")
	DbCon := db.Manager()
	jkn := []model.Jkn{}
	if limit == "" {
		limit = "20"
	}
	if offset == "" {
		offset = "0"
	}
	if order == "" {
		order = "nama desc"
	}

	if kel != "" {
		dbc := DbCon.Debug().Where("kelurahan_id = ?", kel).Limit(limit).Offset(offset).Order(order).Find(&jkn)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kec != "" {
		dbc := DbCon.Debug().Where("kecamatan_id = ?", kec).Limit(limit).Offset(offset).Order(order).Find(&jkn)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kab != "" {
		dbc := DbCon.Debug().Where("kota_kabupaten_id = ?", kab).Limit(limit).Offset(offset).Order(order).Find(&jkn)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if prov != "" {
		dbc := DbCon.Debug().Where("provinsi_id = ?", prov).Limit(limit).Offset(offset).Order(order).Find(&jkn)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else {
		dbc := DbCon.Debug().Limit(limit).Offset(offset).Order(order).Find(&jkn)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	}
	if jkn == nil {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":   "false",
			"message": "Data Kosong",
		})
	} else {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":   false,
			"message": "Data Berhasil di ambil",
			"data":    jkn,
		})
	}
}

func CountGetWilayahJKN(c echo.Context) error {
	prov := c.QueryParam("prov")
	kab := c.QueryParam("kota")
	kec := c.QueryParam("kec")
	kel := c.QueryParam("kel")

	DbCon := db.Manager()
	jkn := []model.Jkn{}
	// var BELUM_MENJADI_PESERTA_JKN int
	// var PESERTA_JKN int
	// var countSurveyIndividu int

	if kel != "" {
		dbc := DbCon.Debug().Raw("SELECT count(survei_individu_detail_id) as survei_individu_detail_id,sum(belum_menjadi_peserta_jkn) as belum_menjadi_peserta_jkn, sum(peserta_jkn) as peserta_jkn FROM jkn kelurahan_id=?",kel).Scan(&jkn)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kec != "" {
		dbc := DbCon.Debug().Raw("SELECT count(survei_individu_detail_id) as survei_individu_detail_id,sum(belum_menjadi_peserta_jkn) as belum_menjadi_peserta_jkn, sum(peserta_jkn) as peserta_jkn FROM jkn kecamatan_id=?",kec).Scan(&jkn)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kab != "" {
		dbc := DbCon.Debug().Raw("SELECT count(survei_individu_detail_id) as survei_individu_detail_id,sum(belum_menjadi_peserta_jkn) as belum_menjadi_peserta_jkn, sum(peserta_jkn) as peserta_jkn FROM jkn where kota_kabupaten_id=?",kab).Scan(&jkn)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if prov != "" {
		dbc := DbCon.Debug().Raw("SELECT count(survei_individu_detail_id) as survei_individu_detail_id,sum(belum_menjadi_peserta_jkn) as belum_menjadi_peserta_jkn, sum(peserta_jkn) as peserta_jkn FROM jkn where provinsi_id=?",prov).Scan(&jkn)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else {
		dbc := DbCon.Debug().Raw("SELECT count(survei_individu_detail_id) as survei_individu_detail_id,sum(belum_menjadi_peserta_jkn) as belum_menjadi_peserta_jkn, sum(peserta_jkn) as peserta_jkn FROM jkn").Scan(&jkn)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	}
	if jkn == nil {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":   "false",
			"message": "Data Kosong",
		})
	} else {
		
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":                            false,
			"message":                          "Data Berhasil di ambil",
			"jumlah_survei_individu_detail_id": jkn[0].Survei_individu_detail_id,
			"jumlah_peserta_jkn":               jkn[0].Peserta_jkn,
			"jumlah_belum_menjadi_peserta_jkn": jkn[0].Belum_menjadi_peserta_jkn,
		})
	}
}

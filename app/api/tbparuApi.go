package api

import (
	"ApiDashboard/db"
	"ApiDashboard/model"
	"net/http"

	"github.com/labstack/echo/v4"
)

func GetWilayahTBParu(c echo.Context) error {
	prov := c.QueryParam("prov")
	kab := c.QueryParam("kota")
	kec := c.QueryParam("kec")
	kel := c.QueryParam("kel")
	limit := c.QueryParam("limit")
	offset := c.QueryParam("offset")
	order := c.QueryParam("order")
	DbCon := db.Manager()
	tbparu := []model.TbParu{}
	if limit == "" {
		limit = "20"
	}
	if offset == "" {
		offset = "0"
	}
	if order == "" {
		order = "nama desc"
	}
	if kel != "" {
		dbc := DbCon.Debug().Where("kelurahan_id = ?", kel).Limit(limit).Offset(offset).Order(order).Find(&tbparu)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kec != "" {
		dbc := DbCon.Debug().Where("kecamatan_id = ?", kec).Limit(limit).Offset(offset).Order(order).Find(&tbparu)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kab != "" {
		dbc := DbCon.Debug().Where("kota_kabupaten_id = ?", kab).Limit(limit).Offset(offset).Order(order).Find(&tbparu)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if prov != "" {
		dbc := DbCon.Debug().Where("provinsi_id = ?", prov).Limit(limit).Offset(offset).Order(order).Find(&tbparu)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else {
		dbc := DbCon.Debug().Limit(limit).Offset(offset).Order(order).Find(&tbparu)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	}
	if tbparu == nil {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":   "false",
			"message": "Data Kosong",
		})
	} else {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":   false,
			"message": "Data Berhasil di ambil",
			"data":    tbparu,
		})
	}
}

func CountGetWilayahTBParu(c echo.Context) error {
	// var Survei_individu_detail_id int
	// var Lebih_dari_sama_dengan_15_TAHUN int
	// var DIDIAGNOSIS_TB int
	// var DIDIAGNOSIS_TAPI_TIDAK_MINUM_OBAT_TB int
	// var PENDERITA_TB_YANG_MINUM_OBAT_SESUAI_STANDAR int
	// var sum(suspek_tb) as suspek_tb int
	prov := c.QueryParam("prov")
	kab := c.QueryParam("kota")
	kec := c.QueryParam("kec")
	kel := c.QueryParam("kel")

	DbCon := db.Manager()
	tbparu := []model.TbParu{}

	if kel != "" {
		dbc := DbCon.Debug().Raw("select count(survei_individu_detail_id) as survei_individu_detail_id, sum(lebih_dari_sama_dengan_15_tahun) as lebih_dari_sama_dengan_15_tahun, sum(didiagnosis_tb) as didiagnosis_tb, sum(didiagnosis_tapi_tidak_minum_obat_tb) as didiagnosis_tapi_tidak_minum_obat_tb, sum(penderita_tb_yang_minum_obat_sesuai_standar) as penderita_tb_yang_minum_obat_sesuai_standar, sum(suspek_tb) as suspek_tb from tb_paru where kelurahan_id=?",kel).Scan(&tbparu)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kec != "" {
		dbc := DbCon.Debug().Raw("select count(survei_individu_detail_id) as survei_individu_detail_id, sum(lebih_dari_sama_dengan_15_tahun) as lebih_dari_sama_dengan_15_tahun, sum(didiagnosis_tb) as didiagnosis_tb, sum(didiagnosis_tapi_tidak_minum_obat_tb) as didiagnosis_tapi_tidak_minum_obat_tb, sum(penderita_tb_yang_minum_obat_sesuai_standar) as penderita_tb_yang_minum_obat_sesuai_standar, sum(suspek_tb) as suspek_tb from tb_paru where kecamatan_id=?",kec).Scan(&tbparu)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kab != "" {
		dbc := DbCon.Debug().Raw("select count(survei_individu_detail_id) as survei_individu_detail_id, sum(lebih_dari_sama_dengan_15_tahun) as lebih_dari_sama_dengan_15_tahun, sum(didiagnosis_tb) as didiagnosis_tb, sum(didiagnosis_tapi_tidak_minum_obat_tb) as didiagnosis_tapi_tidak_minum_obat_tb, sum(penderita_tb_yang_minum_obat_sesuai_standar) as penderita_tb_yang_minum_obat_sesuai_standar, sum(suspek_tb) as suspek_tb from tb_paru where kota_kabupaten_id=?",kab).Scan(&tbparu)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if prov != "" {
		dbc := DbCon.Debug().Raw("select count(survei_individu_detail_id) as survei_individu_detail_id, sum(lebih_dari_sama_dengan_15_tahun) as lebih_dari_sama_dengan_15_tahun, sum(didiagnosis_tb) as didiagnosis_tb, sum(didiagnosis_tapi_tidak_minum_obat_tb) as didiagnosis_tapi_tidak_minum_obat_tb, sum(penderita_tb_yang_minum_obat_sesuai_standar) as penderita_tb_yang_minum_obat_sesuai_standar, sum(suspek_tb) as suspek_tb from tb_paru where provinsi_id=?",prov).Scan(&tbparu)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else {
		dbc := DbCon.Debug().Raw("select count(survei_individu_detail_id) as survei_individu_detail_id, sum(lebih_dari_sama_dengan_15_tahun) as lebih_dari_sama_dengan_15_tahun, sum(didiagnosis_tb) as didiagnosis_tb, sum(didiagnosis_tapi_tidak_minum_obat_tb) as didiagnosis_tapi_tidak_minum_obat_tb, sum(penderita_tb_yang_minum_obat_sesuai_standar) as penderita_tb_yang_minum_obat_sesuai_standar, sum(suspek_tb) as suspek_tb from tb_paru").Scan(&tbparu)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	}
	if tbparu == nil {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":   "false",
			"message": "Data Kosong",
		})
	} else {
		
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":                                              false,
			"message":                                            "Data Berhasil di ambil",
			"jumlah_survei_individu_detail_id":                   tbparu[0].Survei_individu_detail_id,
			"jumlah_lebih_dari_sama_dengan_15_TAHUN":             tbparu[0].Lebih_dari_sama_dengan_15_tahun,
			"jumlah_didiagnosis_TB":                              tbparu[0].Didiagnosis_tb,
			"jumlah_didiagnosis_Tapi_Tidak_Minum_Obat_TB":        tbparu[0].Didiagnosis_tapi_tidak_minum_obat_tb,
			"jumlah_penderita_TB_yang_minum_obat_sesuai_standar": tbparu[0].Penderita_tb_yang_minum_obat_sesuai_standar,
			"jumlah_suspek_TB":                                   tbparu[0].Suspek_tb,
		})
	}
}

package api

import (
	"ApiDashboard/db"
	"ApiDashboard/model"
	"net/http"

	"github.com/labstack/echo/v4"
)

func GetWilayahTumbuhKembang(c echo.Context) error {
	prov := c.QueryParam("prov")
	kab := c.QueryParam("kota")
	kec := c.QueryParam("kec")
	kel := c.QueryParam("kel")
	limit := c.QueryParam("limit")
	offset := c.QueryParam("offset")
	order := c.QueryParam("order")
	DbCon := db.Manager()
	tkembang := []model.Tumbuhkembang{}
	if limit == "" {
		limit = "20"
	}
	if offset == "" {
		offset = "0"
	}
	if order == "" {
		order = "nama desc"
	}

	if kel != "" {
		dbc := DbCon.Debug().Where("kelurahan_id = ?", kel).Limit(limit).Offset(offset).Order(order).Find(&tkembang)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kec != "" {
		dbc := DbCon.Debug().Where("kecamatan_id = ?", kec).Limit(limit).Offset(offset).Order(order).Find(&tkembang)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kab != "" {
		dbc := DbCon.Debug().Where("kota_kabupaten_id = ?", kab).Limit(limit).Offset(offset).Order(order).Find(&tkembang)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if prov != "" {
		dbc := DbCon.Debug().Where("provinsi_id = ?", prov).Limit(limit).Offset(offset).Order(order).Find(&tkembang)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else {
		dbc := DbCon.Debug().Limit(limit).Offset(offset).Order(order).Find(&tkembang)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	}
	if tkembang == nil {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":   "false",
			"message": "Data Kosong",
		})
	} else {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":   false,
			"message": "Data Berhasil di ambil",
			"data":    tkembang,
		})
	}
}

func CountGetWilayahTumbuhKembang(c echo.Context) error {
	// var Survei_individu_detail_id int
	// var USIA_2_sd_59_BULAN int
	// var SASARAN_TIDAK_PEMANTAUAN_PERTUMBUHAN int
	prov := c.QueryParam("prov")
	kab := c.QueryParam("kota")
	kec := c.QueryParam("kec")
	kel := c.QueryParam("kel")

	DbCon := db.Manager()
	tkembang := []model.Tumbuhkembang{}

	if kel != "" {
		dbc := DbCon.Debug().Raw("select count(survei_individu_detail_id) as survei_individu_detail_id,sum(usia_2_sd_59_bulan) as usia_2_sd_59_bulan,sum(sasaran_tidak_pemantauan_pertumbuhan) as sasaran_tidak_pemantauan_pertumbuhan from tumbuh_kembang where kelurahan_id=?",kel).Scan(&tkembang)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kec != "" {
		dbc := DbCon.Debug().Raw("select count(survei_individu_detail_id) as survei_individu_detail_id,sum(usia_2_sd_59_bulan) as usia_2_sd_59_bulan,sum(sasaran_tidak_pemantauan_pertumbuhan) as sasaran_tidak_pemantauan_pertumbuhan from tumbuh_kembang where kecamatan_id=?",kec).Scan(&tkembang)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kab != "" {
		dbc := DbCon.Debug().Raw("select count(survei_individu_detail_id) as survei_individu_detail_id,sum(usia_2_sd_59_bulan) as usia_2_sd_59_bulan,sum(sasaran_tidak_pemantauan_pertumbuhan) as sasaran_tidak_pemantauan_pertumbuhan from tumbuh_kembang where kota_kabupaten_id=?",kab).Scan(&tkembang)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if prov != "" {
		dbc := DbCon.Debug().Raw("select count(survei_individu_detail_id) as survei_individu_detail_id,sum(usia_2_sd_59_bulan) as usia_2_sd_59_bulan,sum(sasaran_tidak_pemantauan_pertumbuhan) as sasaran_tidak_pemantauan_pertumbuhan from tumbuh_kembang where provinsi_id=?",prov).Scan(&tkembang)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else {
		dbc := DbCon.Debug().Raw("select count(survei_individu_detail_id) as survei_individu_detail_id,sum(usia_2_sd_59_bulan) as usia_2_sd_59_bulan,sum(sasaran_tidak_pemantauan_pertumbuhan) as sasaran_tidak_pemantauan_pertumbuhan from tumbuh_kembang").Scan(&tkembang)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	}
	if tkembang == nil {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":   "false",
			"message": "Data Kosong",
		})
	} else {
		
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":                            false,
			"message":                          "Data Berhasil di ambil",
			"jumlah_survei_individu_detail_id": tkembang[0].Survei_individu_detail_id,
			"jumlah_usia_2_sd_59_bulan":        tkembang[0].Usia_2_sd_59_bulan,
			"jumlah_sasaran_tidak_pemantauan_pertumbuhan": tkembang[0].Sasaran_tidak_pemantauan_pertumbuhan,
		})
	}
}

package api

import (
	"ApiDashboard/db"
	"ApiDashboard/model"
	"net/http"

	"github.com/labstack/echo/v4"
)

func GetWilayahHipertensi(c echo.Context) error {
	prov := c.QueryParam("prov")
	kab := c.QueryParam("kota")
	kec := c.QueryParam("kec")
	kel := c.QueryParam("kel")
	limit := c.QueryParam("limit")
	offset := c.QueryParam("offset")
	order := c.QueryParam("order")
	DbCon := db.Manager()
	hipertensi := []model.Hipertensi{}
	if limit == "" {
		limit = "20"
	}
	if offset == "" {
		offset = "0"
	}
	if order == "" {
		order = "nama desc"
	}

	if kel != "" {
		dbc := DbCon.Debug().Where("kelurahan_id = ?", kel).Limit(limit).Offset(offset).Order(order).Find(&hipertensi)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kec != "" {
		dbc := DbCon.Debug().Where("kecamatan_id = ?", kec).Limit(limit).Offset(offset).Order(order).Find(&hipertensi)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kab != "" {
		dbc := DbCon.Debug().Where("kota_kabupaten_id = ?", kab).Limit(limit).Offset(offset).Order(order).Find(&hipertensi)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if prov != "" {
		dbc := DbCon.Debug().Where("provinsi_id = ?", prov).Limit(limit).Offset(offset).Order(order).Find(&hipertensi)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else {
		dbc := DbCon.Debug().Limit(limit).Offset(offset).Order(order).Find(&hipertensi)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	}
	if hipertensi == nil {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":   "false",
			"message": "Data Kosong",
		})
	} else {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":   false,
			"message": "Data Berhasil di ambil",
			"data":    hipertensi,
		})
	}
}

func CountGetWilayahHipertensi(c echo.Context) error {
	// var DIDIAGNOSIS_HIPERTENSI int
	// var INDIVIDU_DIDIAGNOSIS_TAPI_TIDAK_MINUM_OBAT_HIPERTENSI int
	// var INDIVIDU_DIDIAGNOSIS_HIPERTENSI_MINUM_OBAT_SESUAI_STANDAR int
	// var DIUKUR_TEKANAN_DARAH int
	// // var	SISTOL                                                    int
	// // var	DIASTOL                                                   int
	// var SUSPEK_TEKANAN_DARAH_TINGGI int
	// var countSurveyIndividu int
	prov := c.QueryParam("prov")
	kab := c.QueryParam("kota")
	kec := c.QueryParam("kec")
	kel := c.QueryParam("kel")
	DbCon := db.Manager()
	hipertensi := []model.Hipertensi{}

	if kel != "" {
		dbc := DbCon.Debug().Select("didiagnosis_hipertensi,individu_didiagnosis_tapi_tidak_minum_obat_hipertensi,individu_didiagnosis_hipertensi_minum_obat_sesuai_standar,diukur_tekanan_darah,suspek_tekanan_darah_tinggi,survei_individu_detail_id").Where("kelurahan_id = ?", kel).Find(&hipertensi)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	} else if kec != "" {
		dbc := DbCon.Debug().Select("didiagnosis_hipertensi,individu_didiagnosis_tapi_tidak_minum_obat_hipertensi,individu_didiagnosis_hipertensi_minum_obat_sesuai_standar,diukur_tekanan_darah,suspek_tekanan_darah_tinggi,survei_individu_detail_id").Where("kecamatan_id = ?", kec).Find(&hipertensi)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}

	} else if kab != "" {
		dbc := DbCon.Debug().Select("didiagnosis_hipertensi,individu_didiagnosis_tapi_tidak_minum_obat_hipertensi,individu_didiagnosis_hipertensi_minum_obat_sesuai_standar,diukur_tekanan_darah,suspek_tekanan_darah_tinggi,survei_individu_detail_id").Where("kota_kabupaten_id = ?", kab).Find(&hipertensi)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}

	} else if prov != "" {
		dbc := DbCon.Debug().Select("didiagnosis_hipertensi,individu_didiagnosis_tapi_tidak_minum_obat_hipertensi,individu_didiagnosis_hipertensi_minum_obat_sesuai_standar,diukur_tekanan_darah,suspek_tekanan_darah_tinggi,survei_individu_detail_id").Where("provinsi_id = ?", prov).Find(&hipertensi)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}

	} else {
		dbc := DbCon.Debug().Raw("SELECT Count(survei_individu_detail_id) as survei_individu_detail_id,sum(didiagnosis_hipertensi) as didiagnosis_hipertensi, sum(individu_didiagnosis_tapi_tidak_minum_obat_hipertensi) as individu_didiagnosis_tapi_tidak_minum_obat_hipertensi, sum(individu_didiagnosis_hipertensi_minum_obat_sesuai_standar) as individu_didiagnosis_hipertensi_minum_obat_sesuai_standar, sum(diukur_tekanan_darah) as diukur_tekanan_darah, sum(suspek_tekanan_darah_tinggi) as suspek_tekanan_darah_tinggi FROM hipertensi").Find(&hipertensi)
		if dbc.Error != nil {
			return c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"error":   "false",
				"message": "Kesalahan Sementara",
			})
		}
	}
	if hipertensi == nil {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":   "false",
			"message": "Data Kosong",
		})
	} else {

		return c.JSON(http.StatusOK, map[string]interface{}{
			"error":                            false,
			"message":                          "Data Berhasil di ambil",
			"jumlah_survei_individu_detail_id": hipertensi[0].Survei_individu_detail_id,
			"jumlah_didiagnosis_hipertensi":    hipertensi[0].Didiagnosis_hipertensi,
			"jumlah_individu_didiaagnosis_tapi_tidak_minum_obat_hipertensi":     hipertensi[0].Individu_didiagnosis_tapi_tidak_minum_obat_hipertensi,
			"jumlah_individu_didiaagnosis_hipertensi_minum_obat_sesuai_standar": hipertensi[0].Individu_didiagnosis_hipertensi_minum_obat_sesuai_standar,
			"jumlah_diukur_tekanan_darah":                                       hipertensi[0].Diukur_tekanan_darah,
			"jumlah_suspek_tekanan_darah_tinggi":                                hipertensi[0].Suspek_tekanan_darah_tinggi,
		})
	}
}

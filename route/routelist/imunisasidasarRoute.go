package routelist

import (
	"ApiDashboard/app/api"

	"github.com/labstack/echo/v4"
)

func ImunisasidasarRoute(e *echo.Group) {
	e.GET("/getbywilayah", api.GetWilayahImunisasiDasar)
	e.GET("/countgetbywilayah", api.CountGetWilayahImunisasiDasar)
}
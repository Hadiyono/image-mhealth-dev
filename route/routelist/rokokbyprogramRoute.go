package routelist

import (
	"ApiDashboard/app/api"

	"github.com/labstack/echo/v4"
)

func RokokbyprogramRoute(e *echo.Group) {
	e.GET("/getbywilayah", api.GetWilayahRokokbyProgram)
	e.GET("/countgetbywilayah", api.CountGetWilayahRokokbyProgram)
}
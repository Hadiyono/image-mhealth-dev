package routelist

import (
	"ApiDashboard/app/api"

	"github.com/labstack/echo/v4"
)

func AsiEsklusifRoute(e *echo.Group) {
	e.GET("/getbywilayah", api.GetWilayahAsiEsk)
	e.GET("/countgetbywilayah", api.CountGetWilayahAsiEsk)
	e.GET("/tesgetbywilayah", api.TestCoutAsiEsk)
}
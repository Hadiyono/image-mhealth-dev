package routelist

import (
	"ApiDashboard/app/api"

	"github.com/labstack/echo/v4"
)

func RokoksRoute(e *echo.Group) {
	e.GET("/getbywilayah", api.GetWilayahRokoks)
	e.GET("/countgetbywilayah", api.CountGetWilayahRokoks)
}
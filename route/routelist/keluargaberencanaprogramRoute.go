package routelist

import (
	"ApiDashboard/app/api"

	"github.com/labstack/echo/v4"
)

func KeluargaBerencanaprogramRoute(e *echo.Group) {
	e.GET("/getbywilayah", api.GetWilayahKBByProgram)
	e.GET("/countgetbywilayah", api.CountGetWilayahKBByProgram)
}
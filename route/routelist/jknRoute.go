package routelist

import (
	"ApiDashboard/app/api"

	"github.com/labstack/echo/v4"
)

func JknRoute(e *echo.Group) {
	e.GET("/getbywilayah", api.GetWilayahJKN)
	e.GET("/countgetbywilayah", api.CountGetWilayahJKN)
}
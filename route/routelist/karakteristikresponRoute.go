package routelist

import (
	"ApiDashboard/app/api"

	"github.com/labstack/echo/v4"
)

func KarakteristikresponRoute(e *echo.Group) {
	e.GET("/getbywilayah", api.GetWilayahKarakterRes)
	e.GET("/countgetbywilayah", api.CountGetWilayahKarakterRes)
	e.GET("/tesgetbywilayah", api.TesCoutKarakterRes)
}
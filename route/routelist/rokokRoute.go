package routelist

import (
	"ApiDashboard/app/api"

	"github.com/labstack/echo/v4"
)

func RokokRoute (e *echo.Group) {
	e.GET("/getbywilayah", api.GetWilayahRK)
	e.GET("/countgetbywilayah", api.CountGetWilayahRK)
}
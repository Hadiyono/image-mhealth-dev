package routelist

import (
	"ApiDashboard/app/api"

	"github.com/labstack/echo/v4"
)

func SPMRoute(e *echo.Group){
	e.GET("/getbywilayah", api.GetWilayahSpm)
	e.GET("/countgetbywilayah", api.CountGetWilayahSpm)
}
package routelist

import (
	"ApiDashboard/app/api"

	"github.com/labstack/echo/v4"
)

func RokokByProgramUmurRoute(e *echo.Group) {
	e.GET("/getbywilayah", api.GetWilayahRokokbyProgramUmur)
	e.GET("/countgetbywilayah", api.CountGetWilayahRokokbyProgramUmur)
}
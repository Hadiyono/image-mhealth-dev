package routelist

import (
	"ApiDashboard/app/api"

	"github.com/labstack/echo/v4"
)

func PerilakuJambanRoute(e *echo.Group) {
	e.GET("/getbywilayah", api.GetWilayahPJamban)
	e.GET("/countgetbywilayah", api.CountGetWilayahPJamban)
}
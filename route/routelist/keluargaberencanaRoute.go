package routelist

import (
	"ApiDashboard/app/api"

	"github.com/labstack/echo/v4"
)

func KeluargaberencanaRoute(e *echo.Group) {
	e.GET("/getbywilayah", api.GetWilayahKB)
	e.GET("/countgetbywilayah", api.CountGetWilayahKB)
}
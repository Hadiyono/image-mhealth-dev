package routelist

import (
	"ApiDashboard/app/api"

	"github.com/labstack/echo/v4"
)

func TumbuhkembangRoute(e *echo.Group) {
	e.GET("/getbywilayah", api.GetWilayahTumbuhKembang)
	e.GET("/countgetbywilayah", api.CountGetWilayahTumbuhKembang)
}
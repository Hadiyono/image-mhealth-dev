package routelist

import (
	"ApiDashboard/app/api"

	"github.com/labstack/echo/v4"
)

func PerilakuSabRoute(e *echo.Group) {
	e.GET("/getbywilayah", api.GetWilayahPsab)
	e.GET("/countgetbywilayah", api.CountGetWilayahPsab)
}
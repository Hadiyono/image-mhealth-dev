package routelist

import (
	"ApiDashboard/app/api"

	"github.com/labstack/echo/v4"
)

func DenominatorKB(e *echo.Group) {
	e.GET("/getbywilayah", api.GetWilayahDenoKb)
	e.GET("/countgetbywilayah", api.CountGetWilayahDenoKb)
}
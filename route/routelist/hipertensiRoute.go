package routelist

import (
	"ApiDashboard/app/api"

	"github.com/labstack/echo/v4"
)

func HipertensiRoute(e *echo.Group) {
	e.GET("/getbywilayah", api.GetWilayahHipertensi)
	e.GET("/countgetbywilayah", api.CountGetWilayahHipertensi)
}
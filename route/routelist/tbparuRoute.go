package routelist

import (
	"ApiDashboard/app/api"

	"github.com/labstack/echo/v4"
)

func TbparuRoute(e *echo.Group) {
	e.GET("/getbywilayah", api.GetWilayahTBParu)
	e.GET("/countgetbywilayah", api.CountGetWilayahTBParu)
}
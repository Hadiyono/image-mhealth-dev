package routelist

import (
	"ApiDashboard/app/api"

	"github.com/labstack/echo/v4"
)

func PersalinanfaskesRoute(e *echo.Group) {
	e.GET("/getbywilayah", api.GetWilayahPersalinanFKS)
	e.GET("/countgetbywilayah", api.CountGetWilayahPersalinanFKS)
}
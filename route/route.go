package route

import (
	"ApiDashboard/route/routelist"

	"github.com/labstack/echo/v4"
	"github.com/swaggo/echo-swagger"
	_ "ApiDashboard/docs"
)

func Init() *echo.Echo{
	e := echo.New();

	e.GET("/Swagger/*",echoSwagger.WrapHandler)
	spm	:= e.Group("/api/sasaranlifecyclespm")
	tbparu	:= e.Group("/api/tbparu")
	tumbuhkembang	:= e.Group("/api/tumbuhkembang")
	rokoks	:= e.Group("/api/rokoks")
	rokok := e.Group("/api/rokok")
	rokokbyprogram := e.Group("/api/rokokbyprogram")
	rokokbyprogramumur := e.Group("/api/rokokbyprogramumur")
	persalinanfaskes := e.Group("/api/persalinanfaskes")
	perilakusab := e.Group("/api/perilakusab")
	perilakujamban := e.Group("/api/perilakujamban")
	kb := e.Group("/api/kb")
	kbbyprogram := e.Group("/api/kbbyprogram")
	karaterres := e.Group("/api/karaterres")
	jkn := e.Group("/api/jkn")
	imunisasidasar := e.Group("/api/imunisasidasar")
	hipertensi := e.Group("/api/hipertensi")
	denominatorkb := e.Group("/api/denominatorkb")
	asiesklusif := e.Group("/api/asiesklusif")

	routelist.SPMRoute(spm)
	routelist.TbparuRoute(tbparu)
	routelist.TumbuhkembangRoute(tumbuhkembang)
	routelist.RokoksRoute(rokoks)
	routelist.RokokRoute(rokok)
	routelist.RokokbyprogramRoute(rokokbyprogram)
	routelist.RokokByProgramUmurRoute(rokokbyprogramumur)
	routelist.PersalinanfaskesRoute(persalinanfaskes)
	routelist.PerilakuSabRoute(perilakusab)
	routelist.PerilakuJambanRoute(perilakujamban)
	routelist.KeluargaberencanaRoute(kb)
	routelist.KeluargaBerencanaprogramRoute(kbbyprogram)
	routelist.KarakteristikresponRoute(karaterres)
	routelist.JknRoute(jkn)
	routelist.ImunisasidasarRoute(imunisasidasar)
	routelist.HipertensiRoute(hipertensi)
	routelist.DenominatorKB(denominatorkb)
	routelist.AsiEsklusifRoute(asiesklusif)

	return e
}
package main

import (
	
	// "ApiDashboard/db"
	"ApiDashboard/route"
	"github.com/joho/godotenv"
	"log"
	"os"
)


// @title Dashboard API documentation
// @host localhost:1324
// @BasePath /api
func main() {
	err := godotenv.Load()
  if err != nil {
    log.Fatal("Error loading .env file")
  }

	env := os.Getenv("FOO_ENV")
	if "" == env {
	  env = "prod"
	}

	if "test" == env {
	  env = "local"
	}
	godotenv.Load(".env." + env)
	// db.Init()
	e := route.Init()
	e.Logger.Fatal(e.Start(":1324"))
}
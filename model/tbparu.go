package model

import "time"

type TbParu struct {
	Survei_individu_detail_id string    `json:"survei_individu_detail_id"`
	Survei_rumah_tangga_id    string    `json:"survei_rumah_tangga_id"`
	Survei_id                 string    `json:"survei_id"`
	Provinsi_id               string    `json:"provinsi_id"`
	Nama_provinsi             string    `json:"nama_provinsi"`
	Kota_kabupaten_id         string    `json:"kota_kabupaten_id"`
	Nama_kota_kabupaten       string    `json:"nama_kota_kabupaten"`
	Kecamatan_id              string    `json:"kecamatan_id"`
	Nama_kecamatan            string    `json:"nama_kecamatan"`
	Kelurahan_id              string    `json:"kelurahan_id"`
	Nama_kelurahan            string    `json:"nama_kelurahan"`
	Kd_puskesmas              string    `json:"kd_puskesmas"`
	Nik                       string    `json:"nik"`
	Nama                      string    `json:"nama"`
	Tgl_lahir                 time.Time `json:"tgl_lahir"`
	Jenis_kelamin             string    `json:"jenis_kelamin"`

	Lebih_dari_sama_dengan_15_tahun             int `json:"lebih_dari_sama_dengan_15_tahun"`
	Didiagnosis_tb                              int `json:"didiagnosis_tb"`
	Didiagnosis_tapi_tidak_minum_obat_tb        int `json:"didiagnosis_tapi_tidak_minum_obat_tb"`
	Penderita_tb_yang_minum_obat_sesuai_standar int `json:"penderita_tb_yang_minum_obat_sesuai_standar"`
	Suspek_tb                                   int `json:"suspek_tb"`
}

func (TbParu) TableName() string {
	return "tb_paru"
}

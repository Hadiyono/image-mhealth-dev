package model

import (
	"time"
	// "golang.org/x/text/date"
)

type SasaranLifecycleSpm struct {
	Survei_individu_detail_id string    `json:"survei_individu_detail_id"`
	Survei_rumah_tangga_id    string    `json:"survei_rumah_tangga_id"`
	Survei_id                 string    `json:"survei_id"`
	Provinsi_id               string    `json:"provinsi_id"`
	Nama_provinsi             string    `json:"nama_provinsi"`
	Kota_kabupaten_id         string    `json:"kota_kabupaten_id"`
	Nama_kota_kabupaten       string    `json:"nama_kota_kabupaten"`
	Kecamatan_id              string    `json:"kecamatan_id"`
	Nama_kecamatan            string    `json:"nama_kecamatan"`
	Kelurahan_id              string    `json:"kelurahan_id"`
	Nama_kelurahan            string    `json:"nama_kelurahan"`
	Kd_puskesmas              string    `json:"kd_puskesmas"`
	Nik                       string    `json:"nik"`
	Nama                      string    `json:"nama"`
	Tgl_lahir                 time.Time `json:"tgl_lahir"`
	Jenis_kelamin             string    `json:"jenis_kelamin"`

	// beda
	Hamil                  int `json:"Hamil"`
	Usia_0_sd_59_bulan     int `json:"USIA_0_sd_59_BULAN"`
	Usia_7_sd_15_tahun     int `json:"USIA_7_sd_15_TAHUN"`
	Usia_15_sd_59_tahun    int `json:"USIA_15_sd_59_TAHUN"`
	Usia_lbh_dari_60_tahun int `json:"usia_lbh_dari_60_tahun"`
}

func (SasaranLifecycleSpm) TableName() string {
	return "sasaran_life_cycle_spm"
}

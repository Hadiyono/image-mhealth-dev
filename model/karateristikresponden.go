package model

import "time"

type KarateristikResponden struct {
	Survei_individu_detail_id string    `json:"survei_individu_detail_id"`
	Survei_rumah_tangga_id    string    `json:"survei_rumah_tangga_id"`
	Survei_id                 string    `json:"survei_id"`
	Provinsi_id               string    `json:"provinsi_id"`
	Nama_provinsi             string    `json:"nama_provinsi"`
	Kota_kabupaten_id         string    `json:"kota_kabupaten_id"`
	Nama_kota_kabupaten       string    `json:"nama_kota_kabupaten"`
	Kecamatan_id              string    `json:"kecamatan_id"`
	Nama_kecamatan            string    `json:"nama_kecamatan"`
	Kelurahan_id              string    `json:"kelurahan_id"`
	Nama_kelurahan            string    `json:"nama_kelurahan"`
	Kd_puskesmas              string    `json:"kd_puskesmas"`
	Nik                       string    `json:"nik"`
	Nama                      string    `json:"nama"`
	Tgl_lahir                 time.Time `json:"tgl_lahir"`
	Jenis_kelamin             string    `json:"jenis_kelamin"`

	Umur_hari_ini_tahun  int `json:"umur_hari_ini_tahun"`
	Umur_tahun           int `json:"umur_tahun"`
	Umurbulan            int `json:"umurbulan"`
	Umur_bulan           int `json:"umur_bulan"`
	Kategori_umur_bulan  string `json:"kategori_umur_bulan"`
	Kategori_umur_tahun  string `json:"kategori_umur_tahun"`
	Kategori_pendidikan  string `json:"kategori_pendidikan"`
	Kategori_pekerjaan   string `json:"kategori_pekerjaan"`
	Umur_5_sd_9_tahun    int `json:"umur_5_sd_9_tahun"`
	Umur_10_sd_14_tahun  int `json:"umur_10_sd_14_tahun"`
	Umur_15_24_tahun     int `json:"umur_15_24_tahun"`
	Umur_25_34_tahun     int `json:"umur_25_34_tahun"`
	Umur_35_44_tahun     int `json:"umur_35_44_tahun"`
	Umur_45_54_tahun     int `json:"umur_45_54_tahun"`
	Umur_55_64_tahun     int `json:"umur_55_64_tahun"`
	Umur_65_tahun_keatas int `json:"umur_65_tahun_keatas"`
}

func (KarateristikResponden) TableName() string {
	return "karakteristik_responden"
}

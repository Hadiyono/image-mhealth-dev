package model

import "time"

type DenominatorKb struct {
	Survei_individu_detail_id string    `json:"survei_individu_detail_id"`
	Survei_rumah_tangga_id    string    `json:"survei_rumah_tangga_id"`
	Survei_id                 string    `json:"survei_id"`
	Provinsi_id               string    `json:"provinsi_id"`
	Nama_provinsi             string    `json:"nama_provinsi"`
	Kota_kabupaten_id         string    `json:"kota_kabupaten_id"`
	Nama_kota_kabupaten       string    `json:"nama_kota_kabupaten"`
	Kecamatan_id              string    `json:"kecamatan_id"`
	Nama_kecamatan            string    `json:"nama_kecamatan"`
	Kelurahan_id              string    `json:"kelurahan_id"`
	Nama_kelurahan            string    `json:"nama_kelurahan"`
	Kd_puskesmas              string    `json:"kd_puskesmas"`
	Nik                       string    `json:"nik"`
	Nama                      string    `json:"nama"`
	Tgl_lahir                 time.Time `json:"tgl_lahir"`
	Jenis_kelamin             string    `json:"jenis_kelamin"`
	//rambhan
	Wanita_kawin_tidak_hamil_umur_10_sd_14 int `json:"wanita_kawin_tidak_hamil_umur_10_sd_14"`
	Wanita_kawin_tidak_hamil_umur_15_sd_49 int `json:"wanita_kawin_tidak_hamil_umur_15_sd_49"`
	Wanita_kawin_tidak_hamil_umur_50_sd_54 int `json:"wanita_kawin_tidak_hamil_umur_50_sd_54"`
}

func (DenominatorKb) TableName() string {
	return "denominator_kb"
}

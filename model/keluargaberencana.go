package model

import "time"

type KeluargaBerencana struct {
	Survei_individu_detail_id string    `json:"survei_individu_detail_id"`
	Survei_rumah_tangga_id    string    `json:"survei_rumah_tangga_id"`
	Survei_id                 string    `json:"survei_id"`
	Provinsi_id               string    `json:"provinsi_id"`
	Nama_provinsi             string    `json:"nama_provinsi"`
	Kota_kabupaten_id         string    `json:"kota_kabupaten_id"`
	Nama_kota_kabupaten       string    `json:"nama_kota_kabupaten"`
	Kecamatan_id              string    `json:"kecamatan_id"`
	Nama_kecamatan            string    `json:"nama_kecamatan"`
	Kelurahan_id              string    `json:"kelurahan_id"`
	Nama_kelurahan            string    `json:"nama_kelurahan"`
	Kd_puskesmas              string    `json:"kd_puskesmas"`
	Nik                       string    `json:"nik"`
	Nama                      string    `json:"nama"`
	Tgl_lahir                 time.Time `json:"tgl_lahir"`
	Jenis_kelamin             string    `json:"jenis_kelamin"`

	//tambahan
	Usia_lebih_dari_10_tahun                                int `json:"usia_lebih_dari_10_tahun"`
	Usia_10_sd_54                                           int `json:"usia_10_sd_54"`
	Wanita_usia_10_sd_54_kawin                              int `json:"wanita_usia_10_sd_54_kawin"`
	Pria_usia_lebih_dari_10_tahun_kawin                     int `json:"pria_usia_lebih_dari_10_tahun_kawin"`
	Wanita_usia_10_sd_54_dan_pria_usia_lebih_10_sudah_kawin int `json:"wanita_usia_10_sd_54_dan_pria_usia_lebih_10_sudah_kawin"`
	Wanita_usia_10_sd_54_sudah_kawin_tidak_hamil            int `json:"wanita_usia_10_sd_54_sudah_kawin_tidak_hamil"`
	Wanita_usia_10_sd_54_sudah_kawin_tidak_hamil_ber_kb     int `json:"wanita_usia_10_sd_54_sudah_kawin_tidak_hamil_ber_kb"`
	Wanita_usia_10_sd_54_sudahkawin_tidak_hamil_tidak_berkb int `json:"wanita_usia_10_sd_54_sudahkawin_tidak_hamil_tidak_berkb"`
	Wanita_ber_kb                                           int `json:"wanita_ber_kb"`
	Wanita_tidak_ber_kb                                     int `json:"wanita_tidak_ber_kb"`
}

func (KeluargaBerencana) TableName() string {
	return "keluarga_berencana"
}

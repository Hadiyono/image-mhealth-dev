package model

import "time"

type Hipertensi struct {
	Survei_individu_detail_id string    `json:"survei_individu_detail_id"`
	Survei_rumah_tangga_id    string    `json:"survei_rumah_tangga_id"`
	Survei_id                 string    `json:"survei_id"`
	Provinsi_id               string    `json:"provinsi_id"`
	Nama_provinsi             string    `json:"nama_provinsi"`
	Kota_kabupaten_id         string    `json:"kota_kabupaten_id"`
	Nama_kota_kabupaten       string    `json:"nama_kota_kabupaten"`
	Kecamatan_id              string    `json:"kecamatan_id"`
	Nama_kecamatan            string    `json:"nama_kecamatan"`
	Kelurahan_id              string    `json:"kelurahan_id"`
	Nama_kelurahan            string    `json:"nama_kelurahan"`
	Kd_puskesmas              string    `json:"kd_puskesmas"`
	Nik                       string    `json:"nik"`
	Nama                      string    `json:"nama"`
	Tgl_lahir                 time.Time `json:"tgl_lahir"`
	Jenis_kelamin             string    `json:"jenis_kelamin"`

	Didiagnosis_hipertensi                                    int `json:"di_diagnosis_hipertensi"`
	Individu_didiagnosis_tapi_tidak_minum_obat_hipertensi     int `json:"individu_di_diagnosis_tapi_tidak_minum_obat_hipertensi"`
	Individu_didiagnosis_hipertensi_minum_obat_sesuai_standar int `json:"individu_di_diagnosis_hipertensi_minum_obat_sesuai_standar"`
	Diukur_tekanan_darah                                      int `json:"di_ukur_tekanan_darah"`
	Sistol                                                    int `json:"sistol"`
	Diastol                                                   int `json:"diastol"`
	Suspek_tekanan_darah_tinggi                               int `json:"suspek_tekanan_darah_tinggi"`
}

func (Hipertensi) TableName() string {
	return "hipertensi"
}

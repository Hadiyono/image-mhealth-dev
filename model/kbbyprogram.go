package model

import "time"

type KbbyProgram struct {
	Survei_individu_detail_id string    `json:"survei_individu_detail_id"`
	Survei_rumah_tangga_id    string    `json:"survei_rumah_tangga_id"`
	Survei_id                 string    `json:"survei_id"`
	Provinsi_id               string    `json:"provinsi_id"`
	Nama_provinsi             string    `json:"nama_provinsi"`
	Kota_kabupaten_id         string    `json:"kota_kabupaten_id"`
	Nama_kota_kabupaten       string    `json:"nama_kota_kabupaten"`
	Kecamatan_id              string    `json:"kecamatan_id"`
	Nama_kecamatan            string    `json:"nama_kecamatan"`
	Kelurahan_id              string    `json:"kelurahan_id"`
	Nama_kelurahan            string    `json:"nama_kelurahan"`
	Kd_puskesmas              string    `json:"kd_puskesmas"`
	Nik                       string    `json:"nik"`
	Nama                      string    `json:"nama"`
	Tgl_lahir                 time.Time `json:"tgl_lahir"`
	Jenis_kelamin             string    `json:"jenis_kelamin"`

	Wanita_kawin_tidak_hamil_umur_10_sd_14_ber_sd_kb       int `json:"wanita_kawin_tidak_hamil_umur_10_sd_14_ber_sd_kb"`
	Wanita_kawin_tidak_hamil_umur_15_sd_49_ber_sd_kb       int `json:"wanita_kawin_tidak_hamil_umur_15_sd_49_ber_sd_kb"`
	Wanita_kawin_tidak_hamil_umur_50_sd_54_ber_sd_kb       int `json:"wanita_kawin_tidak_hamil_umur_50_sd_54_ber_sd_kb"`
	Wanita_kawin_tidak_hamil_umur_10_sd_14_tidak_ber_sd_kb int `json:"wanita_kawin_tidak_hamil_umur_10_sd_14_tidak_ber_sd_kb"`
	Wanita_kawin_tidak_hamil_umur_15_sd_49_tidak_ber_sd_kb int `json:"wanita_kawin_tidak_hamil_umur_15_sd_49_tidak_ber_sd_kb"`
	Wanita_kawin_tidak_hamil_umur_50_sd_54_tidak_ber_sd_kb int `json:"wanita_kawin_tidak_hamil_umur_50_sd_54_tidak_ber_sd_kb"`
}

func (KbbyProgram) TableName() string {
	return "kb_by_program"
}
